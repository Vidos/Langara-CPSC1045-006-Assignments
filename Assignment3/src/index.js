function changeRadii(sliderContainer){
    let slider = document.querySelector("#CircleRange");
    let CircleL = document.querySelector("#CircleLeft");
    let CircleR = document.querySelector("#CircleRight");
    let radLeft = Number(slider.value);
    let radRight = 100 - Number(slider.value);
    CircleL.setAttribute("r", radLeft);
    CircleR.setAttribute("r", radRight);
}
window.changeRadii = changeRadii;

function fName(proportionContainer){
    let nameBox = document.querySelector("#faceName");
    let nameLabel = document.querySelector("#nameSpot").innerHTML = nameBox.value;
}

function changeHair(proportionContainer){
    let hColour = document.querySelector("#hairColour");
    let hChange = document.querySelector("#hair");
    hChange.setAttribute("fill", hColour.value);
    let length = document.querySelector("#lengthBox");
    let hChangeL = Number(length.value);


}

    let xLeft = 0;
    let yLeft = 0;
    let xRight = 0;
    let yRight = 0; 

function changeEyes(proportionContainer){
    let eyeX = document.querySelector("#pupX");
    let eyeY = document.querySelector("#pupY");
    let eyeLeft = document.querySelector("#leftEye");
    let eyeRight = document.querySelector("#rightEye");
    
    
    if(Number(eyeX.value) > 14 && Number(eyeY.value) < 5 && Number(eyeX.value) >= 10){
        xLeft = xLeft;
        yLeft = yLeft;
        eyeX = document.querySelector("#pupX").value = Number(xLeft) - 150;
        eyeY = document.querySelector("#pupY").value = Number(yLeft) - 240;
    }
    else if(Number(eyeX.value) < 6 && Number(eyeY.value) < 5 && Number(eyeX.value) <= 10){
        xLeft = xLeft;
        yLeft = yLeft;
        eyeX = document.querySelector("#pupX").value = Number(xLeft) - 150;
        eyeY = document.querySelector("#pupY").value = Number(yLeft) - 240;
    }
    else if(Number(eyeX.value) > 14 && Number(eyeY.value) > 15 && Number(eyeX.value) >= 10){
        xLeft = xLeft;
        yLeft = yLeft;
        eyeX = document.querySelector("#pupX").value = Number(xLeft) - 150;
        eyeY = document.querySelector("#pupY").value = Number(yLeft) - 240;
    }
    else if(Number(eyeX.value) < 6 && Number(eyeY.value) > 15 && Number(eyeX.value) <= 10){
        xLeft = xLeft;
        yLeft = yLeft;
        eyeX = document.querySelector("#pupX").value = Number(xLeft) - 150;
        eyeY = document.querySelector("#pupY").value = Number(yLeft) - 240;
    }
    else{
        xLeft = Number(eyeX.value) + 150;
        yLeft = Number(eyeY.value) + 240;
        xRight = Number(eyeX.value) + 230;
        yRight = Number(eyeY.value) + 240;
    }
    eyeLeft.setAttribute("cx", xLeft);
    eyeLeft.setAttribute("cy", yLeft);
    eyeRight.setAttribute("cx", xRight);
    eyeRight.setAttribute("cy", yRight);
    
    
}

function changeSmile(proportionContainer){
    let mouth = document.querySelector("#smileCh");
    let mouthP = document.querySelector("#smileFrown");
    let state = Number(mouth.value);
    if(state == 2){
        mouthP.setAttribute("points", "160,330 170,320 230,320 240,330");  
    }
    else{
        mouthP.setAttribute("points", "160,310 170,320 230,320 240,310");  
    }
}

window.fName = fName;
window.changeSmile = changeSmile;
window.changeEyes = changeEyes;
window.changeHair = changeHair;