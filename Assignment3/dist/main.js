/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

function changeRadii(sliderContainer){
    let slider = document.querySelector("#CircleRange");
    let CircleL = document.querySelector("#CircleLeft");
    let CircleR = document.querySelector("#CircleRight");
    let radLeft = Number(slider.value);
    let radRight = 100 - Number(slider.value);
    CircleL.setAttribute("r", radLeft);
    CircleR.setAttribute("r", radRight);
}
window.changeRadii = changeRadii;

function fName(proportionContainer){
    let nameBox = document.querySelector("#faceName");
    let nameLabel = document.querySelector("#nameSpot").innerHTML = nameBox.value;
}

function changeHair(proportionContainer){
    let hColour = document.querySelector("#hairColour");
    let hChange = document.querySelector("#hair");
    hChange.setAttribute("fill", hColour.value);
    let length = document.querySelector("#lengthBox");
    let hChangeL = Number(length.value);


}

    let xLeft = 0;
    let yLeft = 0;
    let xRight = 0;
    let yRight = 0; 

function changeEyes(proportionContainer){
    let eyeX = document.querySelector("#pupX");
    let eyeY = document.querySelector("#pupY");
    let eyeLeft = document.querySelector("#leftEye");
    let eyeRight = document.querySelector("#rightEye");
    
    
    if(Number(eyeX.value) > 14 && Number(eyeY.value) < 5 && Number(eyeX.value) >= 10){
        xLeft = xLeft;
        yLeft = yLeft;
        eyeX = document.querySelector("#pupX").value = Number(xLeft) - 150;
        eyeY = document.querySelector("#pupY").value = Number(yLeft) - 240;
    }
    else if(Number(eyeX.value) < 6 && Number(eyeY.value) < 5 && Number(eyeX.value) <= 10){
        xLeft = xLeft;
        yLeft = yLeft;
        eyeX = document.querySelector("#pupX").value = Number(xLeft) - 150;
        eyeY = document.querySelector("#pupY").value = Number(yLeft) - 240;
    }
    else if(Number(eyeX.value) > 14 && Number(eyeY.value) > 15 && Number(eyeX.value) >= 10){
        xLeft = xLeft;
        yLeft = yLeft;
        eyeX = document.querySelector("#pupX").value = Number(xLeft) - 150;
        eyeY = document.querySelector("#pupY").value = Number(yLeft) - 240;
    }
    else if(Number(eyeX.value) < 6 && Number(eyeY.value) > 15 && Number(eyeX.value) <= 10){
        xLeft = xLeft;
        yLeft = yLeft;
        eyeX = document.querySelector("#pupX").value = Number(xLeft) - 150;
        eyeY = document.querySelector("#pupY").value = Number(yLeft) - 240;
    }
    else{
        xLeft = Number(eyeX.value) + 150;
        yLeft = Number(eyeY.value) + 240;
        xRight = Number(eyeX.value) + 230;
        yRight = Number(eyeY.value) + 240;
    }
    eyeLeft.setAttribute("cx", xLeft);
    eyeLeft.setAttribute("cy", yLeft);
    eyeRight.setAttribute("cx", xRight);
    eyeRight.setAttribute("cy", yRight);
    
    
}

function changeSmile(proportionContainer){
    let mouth = document.querySelector("#smileCh");
    let mouthP = document.querySelector("#smileFrown");
    let state = Number(mouth.value);
    if(state == 2){
        mouthP.setAttribute("points", "160,330 170,320 230,320 240,330");  
    }
    else{
        mouthP.setAttribute("points", "160,310 170,320 230,320 240,310");  
    }
}

window.fName = fName;
window.changeSmile = changeSmile;
window.changeEyes = changeEyes;
window.changeHair = changeHair;

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtEQUEwQyxnQ0FBZ0M7QUFDMUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnRUFBd0Qsa0JBQWtCO0FBQzFFO0FBQ0EseURBQWlELGNBQWM7QUFDL0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEQUF5QyxpQ0FBaUM7QUFDMUUsd0hBQWdILG1CQUFtQixFQUFFO0FBQ3JJO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7OztBQUdBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1COztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUU7QUFDQTtBQUNBO0FBQ0EseUU7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLCtCIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9pbmRleC5qc1wiKTtcbiIsImZ1bmN0aW9uIGNoYW5nZVJhZGlpKHNsaWRlckNvbnRhaW5lcil7XHJcbiAgICBsZXQgc2xpZGVyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNDaXJjbGVSYW5nZVwiKTtcclxuICAgIGxldCBDaXJjbGVMID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNDaXJjbGVMZWZ0XCIpO1xyXG4gICAgbGV0IENpcmNsZVIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI0NpcmNsZVJpZ2h0XCIpO1xyXG4gICAgbGV0IHJhZExlZnQgPSBOdW1iZXIoc2xpZGVyLnZhbHVlKTtcclxuICAgIGxldCByYWRSaWdodCA9IDEwMCAtIE51bWJlcihzbGlkZXIudmFsdWUpO1xyXG4gICAgQ2lyY2xlTC5zZXRBdHRyaWJ1dGUoXCJyXCIsIHJhZExlZnQpO1xyXG4gICAgQ2lyY2xlUi5zZXRBdHRyaWJ1dGUoXCJyXCIsIHJhZFJpZ2h0KTtcclxufVxyXG53aW5kb3cuY2hhbmdlUmFkaWkgPSBjaGFuZ2VSYWRpaTtcclxuXHJcbmZ1bmN0aW9uIGZOYW1lKHByb3BvcnRpb25Db250YWluZXIpe1xyXG4gICAgbGV0IG5hbWVCb3ggPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2ZhY2VOYW1lXCIpO1xyXG4gICAgbGV0IG5hbWVMYWJlbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjbmFtZVNwb3RcIikuaW5uZXJIVE1MID0gbmFtZUJveC52YWx1ZTtcclxufVxyXG5cclxuZnVuY3Rpb24gY2hhbmdlSGFpcihwcm9wb3J0aW9uQ29udGFpbmVyKXtcclxuICAgIGxldCBoQ29sb3VyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNoYWlyQ29sb3VyXCIpO1xyXG4gICAgbGV0IGhDaGFuZ2UgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2hhaXJcIik7XHJcbiAgICBoQ2hhbmdlLnNldEF0dHJpYnV0ZShcImZpbGxcIiwgaENvbG91ci52YWx1ZSk7XHJcbiAgICBsZXQgbGVuZ3RoID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNsZW5ndGhCb3hcIik7XHJcbiAgICBsZXQgaENoYW5nZUwgPSBOdW1iZXIobGVuZ3RoLnZhbHVlKTtcclxuXHJcblxyXG59XHJcblxyXG4gICAgbGV0IHhMZWZ0ID0gMDtcclxuICAgIGxldCB5TGVmdCA9IDA7XHJcbiAgICBsZXQgeFJpZ2h0ID0gMDtcclxuICAgIGxldCB5UmlnaHQgPSAwOyBcclxuXHJcbmZ1bmN0aW9uIGNoYW5nZUV5ZXMocHJvcG9ydGlvbkNvbnRhaW5lcil7XHJcbiAgICBsZXQgZXllWCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjcHVwWFwiKTtcclxuICAgIGxldCBleWVZID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNwdXBZXCIpO1xyXG4gICAgbGV0IGV5ZUxlZnQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2xlZnRFeWVcIik7XHJcbiAgICBsZXQgZXllUmlnaHQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3JpZ2h0RXllXCIpO1xyXG4gICAgXHJcbiAgICBcclxuICAgIGlmKE51bWJlcihleWVYLnZhbHVlKSA+IDE0ICYmIE51bWJlcihleWVZLnZhbHVlKSA8IDUgJiYgTnVtYmVyKGV5ZVgudmFsdWUpID49IDEwKXtcclxuICAgICAgICB4TGVmdCA9IHhMZWZ0O1xyXG4gICAgICAgIHlMZWZ0ID0geUxlZnQ7XHJcbiAgICAgICAgZXllWCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjcHVwWFwiKS52YWx1ZSA9IE51bWJlcih4TGVmdCkgLSAxNTA7XHJcbiAgICAgICAgZXllWSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjcHVwWVwiKS52YWx1ZSA9IE51bWJlcih5TGVmdCkgLSAyNDA7XHJcbiAgICB9XHJcbiAgICBlbHNlIGlmKE51bWJlcihleWVYLnZhbHVlKSA8IDYgJiYgTnVtYmVyKGV5ZVkudmFsdWUpIDwgNSAmJiBOdW1iZXIoZXllWC52YWx1ZSkgPD0gMTApe1xyXG4gICAgICAgIHhMZWZ0ID0geExlZnQ7XHJcbiAgICAgICAgeUxlZnQgPSB5TGVmdDtcclxuICAgICAgICBleWVYID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNwdXBYXCIpLnZhbHVlID0gTnVtYmVyKHhMZWZ0KSAtIDE1MDtcclxuICAgICAgICBleWVZID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNwdXBZXCIpLnZhbHVlID0gTnVtYmVyKHlMZWZ0KSAtIDI0MDtcclxuICAgIH1cclxuICAgIGVsc2UgaWYoTnVtYmVyKGV5ZVgudmFsdWUpID4gMTQgJiYgTnVtYmVyKGV5ZVkudmFsdWUpID4gMTUgJiYgTnVtYmVyKGV5ZVgudmFsdWUpID49IDEwKXtcclxuICAgICAgICB4TGVmdCA9IHhMZWZ0O1xyXG4gICAgICAgIHlMZWZ0ID0geUxlZnQ7XHJcbiAgICAgICAgZXllWCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjcHVwWFwiKS52YWx1ZSA9IE51bWJlcih4TGVmdCkgLSAxNTA7XHJcbiAgICAgICAgZXllWSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjcHVwWVwiKS52YWx1ZSA9IE51bWJlcih5TGVmdCkgLSAyNDA7XHJcbiAgICB9XHJcbiAgICBlbHNlIGlmKE51bWJlcihleWVYLnZhbHVlKSA8IDYgJiYgTnVtYmVyKGV5ZVkudmFsdWUpID4gMTUgJiYgTnVtYmVyKGV5ZVgudmFsdWUpIDw9IDEwKXtcclxuICAgICAgICB4TGVmdCA9IHhMZWZ0O1xyXG4gICAgICAgIHlMZWZ0ID0geUxlZnQ7XHJcbiAgICAgICAgZXllWCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjcHVwWFwiKS52YWx1ZSA9IE51bWJlcih4TGVmdCkgLSAxNTA7XHJcbiAgICAgICAgZXllWSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjcHVwWVwiKS52YWx1ZSA9IE51bWJlcih5TGVmdCkgLSAyNDA7XHJcbiAgICB9XHJcbiAgICBlbHNle1xyXG4gICAgICAgIHhMZWZ0ID0gTnVtYmVyKGV5ZVgudmFsdWUpICsgMTUwO1xyXG4gICAgICAgIHlMZWZ0ID0gTnVtYmVyKGV5ZVkudmFsdWUpICsgMjQwO1xyXG4gICAgICAgIHhSaWdodCA9IE51bWJlcihleWVYLnZhbHVlKSArIDIzMDtcclxuICAgICAgICB5UmlnaHQgPSBOdW1iZXIoZXllWS52YWx1ZSkgKyAyNDA7XHJcbiAgICB9XHJcbiAgICBleWVMZWZ0LnNldEF0dHJpYnV0ZShcImN4XCIsIHhMZWZ0KTtcclxuICAgIGV5ZUxlZnQuc2V0QXR0cmlidXRlKFwiY3lcIiwgeUxlZnQpO1xyXG4gICAgZXllUmlnaHQuc2V0QXR0cmlidXRlKFwiY3hcIiwgeFJpZ2h0KTtcclxuICAgIGV5ZVJpZ2h0LnNldEF0dHJpYnV0ZShcImN5XCIsIHlSaWdodCk7XHJcbiAgICBcclxuICAgIFxyXG59XHJcblxyXG5mdW5jdGlvbiBjaGFuZ2VTbWlsZShwcm9wb3J0aW9uQ29udGFpbmVyKXtcclxuICAgIGxldCBtb3V0aCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjc21pbGVDaFwiKTtcclxuICAgIGxldCBtb3V0aFAgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3NtaWxlRnJvd25cIik7XHJcbiAgICBsZXQgc3RhdGUgPSBOdW1iZXIobW91dGgudmFsdWUpO1xyXG4gICAgaWYoc3RhdGUgPT0gMil7XHJcbiAgICAgICAgbW91dGhQLnNldEF0dHJpYnV0ZShcInBvaW50c1wiLCBcIjE2MCwzMzAgMTcwLDMyMCAyMzAsMzIwIDI0MCwzMzBcIik7ICBcclxuICAgIH1cclxuICAgIGVsc2V7XHJcbiAgICAgICAgbW91dGhQLnNldEF0dHJpYnV0ZShcInBvaW50c1wiLCBcIjE2MCwzMTAgMTcwLDMyMCAyMzAsMzIwIDI0MCwzMTBcIik7ICBcclxuICAgIH1cclxufVxyXG5cclxud2luZG93LmZOYW1lID0gZk5hbWU7XHJcbndpbmRvdy5jaGFuZ2VTbWlsZSA9IGNoYW5nZVNtaWxlO1xyXG53aW5kb3cuY2hhbmdlRXllcyA9IGNoYW5nZUV5ZXM7XHJcbndpbmRvdy5jaGFuZ2VIYWlyID0gY2hhbmdlSGFpcjsiXSwic291cmNlUm9vdCI6IiJ9