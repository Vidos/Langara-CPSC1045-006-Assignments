function listen(){
    let X = event.offsetX;
    let Y = event.offsetY;
    let getcolour = document.querySelector("#colourPick");
    let colour = getcolour.value;
    drawCircle();

function drawCircle(){
    let circleString = '<circle cx="' + X + '" cy="' + Y + '" r="10" fill="' + colour + '"/>';
    let output = document.querySelector("#draw");
    output.innerHTML += circleString;
}
}
window.listen = listen;
//Assignment below
let svgStringStartNewPoly = '<polyline points="" stroke="red" fill="none"/>';
let svgStringCurrent = '<polyline points="" stroke="red" fill="none"/>';
let output2 = document.querySelector("#draw2");
output2.innerHTML = svgStringCurrent;

function listenAssignment(){
    let X = event.offsetX;
    let Y = event.offsetY;
    drawPoly(X, Y);
}
function drawPoly(X, Y){
    let element = document.querySelector("polyline:last-of-type").getAttribute("points");
    let polyPoints = element + " " + X + "," + Y;
    element = document.querySelector("polyline:last-of-type");
    element.setAttribute("points", polyPoints);
    svgStringCurrent = element;
}

function buttonClick(){
    output2.innerHTML += svgStringStartNewPoly;
    console.log(output2);
}

 let i = 0;
setInterval(function changeColour(){
    let colour = ["orange", "blue", "green", "red"];
    let lines = document.querySelectorAll("#draw2>polyline");
    let l = 0;
    if(i == colour.length){
        i = 0;
    }
    for(let element of lines){
        lines[l].setAttribute("stroke", colour[i]);
        l += 1;
    }
    i += 1;
}, 100)

window.listenAssignment = listenAssignment;
window.buttonClick = buttonClick;