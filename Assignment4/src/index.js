function update(inputs){
    let X = document.querySelector("#moveX");
    let Y = document.querySelector("#moveY");
    let R = document.querySelector("#moveR");
    let objects = document.querySelector("#collection");
    let xTransform = Number(X.value);
    let yTransform = Number(Y.value);
    let rTransform = Number(R.value);
    let translateString = "translate("+ xTransform + " " + yTransform + ")";
    let rotateString = "rotate("+ rTransform +")";
    let transformString = translateString + " " + rotateString;
    objects.setAttribute("transform", transformString);
}
window.update = update;

function updateStar(inputs){
    let innerR = document.querySelector("#innerRadius");
    let outerR = document.querySelector("#outerRadius");
    let xS = document.querySelector("#moveStarX");
    let yS = document.querySelector("#moveStarY");
    let starR = document.querySelector("#rStar");
    let starControl = document.querySelector("#star");
    let xSTransform = Number(xS.value);
    let ySTransform = Number(yS.value);
    let iRTransform = Number(innerR.value);
    let oRTransform = Number(outerR.value);
    let sRTransform = Number(starR.value);
    let translateString2 = "translate("+ xSTransform + " " + ySTransform + ")";
    let rotateString2 = "rotate("+ sRTransform +")";
    let transformString2 = translateString2 + " " + rotateString2;
    let pointsTransform = oRTransform * Math.cos(0) + "," + oRTransform * Math.sin(0) + " " + iRTransform * Math.cos(0.76) + "," + iRTransform * Math.sin(0.76) + " " + oRTransform * Math.cos(1.57) + "," + oRTransform * Math.sin(1.57) + " " + iRTransform * Math.cos(2.36) + "," + iRTransform * Math.sin(2.36) + " " + oRTransform * Math.cos(3.14) + "," + oRTransform * Math.sin(3.14) + " " + iRTransform * Math.cos(3.92) + "," + iRTransform * Math.sin(3.92) + " " + oRTransform * Math.cos(4.71) + "," + oRTransform * Math.sin(4.71) + " " + iRTransform * Math.cos(5.50) + "," + iRTransform * Math.sin(5.50);
    starControl.setAttribute("points", pointsTransform);
    starControl.setAttribute("transform", transformString2);
    
}
window.updateStar = updateStar;