/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

function update(inputs){
    let X = document.querySelector("#moveX");
    let Y = document.querySelector("#moveY");
    let R = document.querySelector("#moveR");
    let objects = document.querySelector("#collection");
    let xTransform = Number(X.value);
    let yTransform = Number(Y.value);
    let rTransform = Number(R.value);
    let translateString = "translate("+ xTransform + " " + yTransform + ")";
    let rotateString = "rotate("+ rTransform +")";
    let transformString = translateString + " " + rotateString;
    objects.setAttribute("transform", transformString);
}
window.update = update;

function updateStar(inputs){
    let innerR = document.querySelector("#innerRadius");
    let outerR = document.querySelector("#outerRadius");
    let xS = document.querySelector("#moveStarX");
    let yS = document.querySelector("#moveStarY");
    let starR = document.querySelector("#rStar");
    let starControl = document.querySelector("#star");
    let xSTransform = Number(xS.value);
    let ySTransform = Number(yS.value);
    let iRTransform = Number(innerR.value);
    let oRTransform = Number(outerR.value);
    let sRTransform = Number(starR.value);
    let translateString2 = "translate("+ xSTransform + " " + ySTransform + ")";
    let rotateString2 = "rotate("+ sRTransform +")";
    let transformString2 = translateString2 + " " + rotateString2;
    let pointsTransform = oRTransform * Math.cos(0) + "," + oRTransform * Math.sin(0) + " " + iRTransform * Math.cos(0.76) + "," + iRTransform * Math.sin(0.76) + " " + oRTransform * Math.cos(1.57) + "," + oRTransform * Math.sin(1.57) + " " + iRTransform * Math.cos(2.36) + "," + iRTransform * Math.sin(2.36) + " " + oRTransform * Math.cos(3.14) + "," + oRTransform * Math.sin(3.14) + " " + iRTransform * Math.cos(3.92) + "," + iRTransform * Math.sin(3.92) + " " + oRTransform * Math.cos(4.71) + "," + oRTransform * Math.sin(4.71) + " " + iRTransform * Math.cos(5.50) + "," + iRTransform * Math.sin(5.50);
    starControl.setAttribute("points", pointsTransform);
    starControl.setAttribute("transform", transformString2);
    
}
window.updateStar = updateStar;

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtEQUEwQyxnQ0FBZ0M7QUFDMUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnRUFBd0Qsa0JBQWtCO0FBQzFFO0FBQ0EseURBQWlELGNBQWM7QUFDL0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEQUF5QyxpQ0FBaUM7QUFDMUUsd0hBQWdILG1CQUFtQixFQUFFO0FBQ3JJO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7OztBQUdBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLCtCIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9pbmRleC5qc1wiKTtcbiIsImZ1bmN0aW9uIHVwZGF0ZShpbnB1dHMpe1xyXG4gICAgbGV0IFggPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI21vdmVYXCIpO1xyXG4gICAgbGV0IFkgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI21vdmVZXCIpO1xyXG4gICAgbGV0IFIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI21vdmVSXCIpO1xyXG4gICAgbGV0IG9iamVjdHMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2NvbGxlY3Rpb25cIik7XHJcbiAgICBsZXQgeFRyYW5zZm9ybSA9IE51bWJlcihYLnZhbHVlKTtcclxuICAgIGxldCB5VHJhbnNmb3JtID0gTnVtYmVyKFkudmFsdWUpO1xyXG4gICAgbGV0IHJUcmFuc2Zvcm0gPSBOdW1iZXIoUi52YWx1ZSk7XHJcbiAgICBsZXQgdHJhbnNsYXRlU3RyaW5nID0gXCJ0cmFuc2xhdGUoXCIrIHhUcmFuc2Zvcm0gKyBcIiBcIiArIHlUcmFuc2Zvcm0gKyBcIilcIjtcclxuICAgIGxldCByb3RhdGVTdHJpbmcgPSBcInJvdGF0ZShcIisgclRyYW5zZm9ybSArXCIpXCI7XHJcbiAgICBsZXQgdHJhbnNmb3JtU3RyaW5nID0gdHJhbnNsYXRlU3RyaW5nICsgXCIgXCIgKyByb3RhdGVTdHJpbmc7XHJcbiAgICBvYmplY3RzLnNldEF0dHJpYnV0ZShcInRyYW5zZm9ybVwiLCB0cmFuc2Zvcm1TdHJpbmcpO1xyXG59XHJcbndpbmRvdy51cGRhdGUgPSB1cGRhdGU7XHJcblxyXG5mdW5jdGlvbiB1cGRhdGVTdGFyKGlucHV0cyl7XHJcbiAgICBsZXQgaW5uZXJSID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNpbm5lclJhZGl1c1wiKTtcclxuICAgIGxldCBvdXRlclIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI291dGVyUmFkaXVzXCIpO1xyXG4gICAgbGV0IHhTID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNtb3ZlU3RhclhcIik7XHJcbiAgICBsZXQgeVMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI21vdmVTdGFyWVwiKTtcclxuICAgIGxldCBzdGFyUiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjclN0YXJcIik7XHJcbiAgICBsZXQgc3RhckNvbnRyb2wgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3N0YXJcIik7XHJcbiAgICBsZXQgeFNUcmFuc2Zvcm0gPSBOdW1iZXIoeFMudmFsdWUpO1xyXG4gICAgbGV0IHlTVHJhbnNmb3JtID0gTnVtYmVyKHlTLnZhbHVlKTtcclxuICAgIGxldCBpUlRyYW5zZm9ybSA9IE51bWJlcihpbm5lclIudmFsdWUpO1xyXG4gICAgbGV0IG9SVHJhbnNmb3JtID0gTnVtYmVyKG91dGVyUi52YWx1ZSk7XHJcbiAgICBsZXQgc1JUcmFuc2Zvcm0gPSBOdW1iZXIoc3RhclIudmFsdWUpO1xyXG4gICAgbGV0IHRyYW5zbGF0ZVN0cmluZzIgPSBcInRyYW5zbGF0ZShcIisgeFNUcmFuc2Zvcm0gKyBcIiBcIiArIHlTVHJhbnNmb3JtICsgXCIpXCI7XHJcbiAgICBsZXQgcm90YXRlU3RyaW5nMiA9IFwicm90YXRlKFwiKyBzUlRyYW5zZm9ybSArXCIpXCI7XHJcbiAgICBsZXQgdHJhbnNmb3JtU3RyaW5nMiA9IHRyYW5zbGF0ZVN0cmluZzIgKyBcIiBcIiArIHJvdGF0ZVN0cmluZzI7XHJcbiAgICBsZXQgcG9pbnRzVHJhbnNmb3JtID0gb1JUcmFuc2Zvcm0gKiBNYXRoLmNvcygwKSArIFwiLFwiICsgb1JUcmFuc2Zvcm0gKiBNYXRoLnNpbigwKSArIFwiIFwiICsgaVJUcmFuc2Zvcm0gKiBNYXRoLmNvcygwLjc2KSArIFwiLFwiICsgaVJUcmFuc2Zvcm0gKiBNYXRoLnNpbigwLjc2KSArIFwiIFwiICsgb1JUcmFuc2Zvcm0gKiBNYXRoLmNvcygxLjU3KSArIFwiLFwiICsgb1JUcmFuc2Zvcm0gKiBNYXRoLnNpbigxLjU3KSArIFwiIFwiICsgaVJUcmFuc2Zvcm0gKiBNYXRoLmNvcygyLjM2KSArIFwiLFwiICsgaVJUcmFuc2Zvcm0gKiBNYXRoLnNpbigyLjM2KSArIFwiIFwiICsgb1JUcmFuc2Zvcm0gKiBNYXRoLmNvcygzLjE0KSArIFwiLFwiICsgb1JUcmFuc2Zvcm0gKiBNYXRoLnNpbigzLjE0KSArIFwiIFwiICsgaVJUcmFuc2Zvcm0gKiBNYXRoLmNvcygzLjkyKSArIFwiLFwiICsgaVJUcmFuc2Zvcm0gKiBNYXRoLnNpbigzLjkyKSArIFwiIFwiICsgb1JUcmFuc2Zvcm0gKiBNYXRoLmNvcyg0LjcxKSArIFwiLFwiICsgb1JUcmFuc2Zvcm0gKiBNYXRoLnNpbig0LjcxKSArIFwiIFwiICsgaVJUcmFuc2Zvcm0gKiBNYXRoLmNvcyg1LjUwKSArIFwiLFwiICsgaVJUcmFuc2Zvcm0gKiBNYXRoLnNpbig1LjUwKTtcclxuICAgIHN0YXJDb250cm9sLnNldEF0dHJpYnV0ZShcInBvaW50c1wiLCBwb2ludHNUcmFuc2Zvcm0pO1xyXG4gICAgc3RhckNvbnRyb2wuc2V0QXR0cmlidXRlKFwidHJhbnNmb3JtXCIsIHRyYW5zZm9ybVN0cmluZzIpO1xyXG4gICAgXHJcbn1cclxud2luZG93LnVwZGF0ZVN0YXIgPSB1cGRhdGVTdGFyOyJdLCJzb3VyY2VSb290IjoiIn0=