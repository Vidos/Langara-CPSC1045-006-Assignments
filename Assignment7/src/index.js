function page(){
    let pointNum = document.querySelector("#sides");
    let points = Number(pointNum.value);
    let innerRad = 50;
    let outerRad = 50;
    let xOut = 0;
    let yOut = 0;
    let xIn = 0;
    let yIn = 0;
    let pointList = [];
    let pos = 0;
    let innerPos = 0;
    let outerPos = 0;
    for(let i = 0; i <=points; i++){
        
        makePoints();
        pos += 1;
        }
    drawPoints();

function makePoints(){
    
    if(pos > 0){
      outerPos += 1;
      if(outerPos <= points/2){
        xOut = 50* Math.floor(Math.cos((360/points)*outerPos)) + 100;
        yOut = 50* Math.floor(Math.sin((360/points)*outerPos)) + 100;
      }
      else{
        xOut = 50* Math.floor(Math.cos((360/points)*outerPos));
        yOut = 50* Math.floor(Math.sin((360/points)*outerPos));
      }
      
       
        pointList.push({X:xOut, Y:yOut});
        console.log(pointList);
        console.log(pos);
    }
    else if(pos == 9999){
        innerPos += 1;
        if(innerPos <= (points/2)){
            xIn = Math.abs(innerRad * Math.cos(360/(2*points))) + xOut;
            
            yIn += yOut + (yOut/2);
        }
        else if(innerPos > (points/2)){
            xIn = innerRad * Math.cos(360/(2*points)) + xOut;
            yOut += Math.abs(2*yOut);
            yIn = yIn;
        }
        pointList.push({X:xIn, Y:yIn});
        console.log(pos);
        console.log(pointList);
    }
    
   
}

function drawPoints(){
    let output = document.querySelector("#output");
    let svgString = '<svg width="' + '400"' + 'height="' + '400"' + '>';
    let starString =  '<polygon points="';
    pos = 0;
    for(element of pointList){
        starString += + pointList[pos]["X"] + ',' + pointList[pos]["Y"] + ' ';
        pos += 1;
    }
    starString += '" ' + 'stroke="red" fill="black" />';
    svgString += starString + '</svg>';
    console.log(starString);
    output.innerHTML = svgString;
}
}
window.page = page;