/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

let blockList = [{ block: '<rect id="testBlock" x="0" y="0" width="50" height="50" stroke="black" fill="none"/>', state: "falling", size: "S" }
];
let svgString = '<rect width="400" height="800" stroke="black" fill="none"/>';
let currentColumn = 1;
let currentRow = 16;
let columns = [];
let rows = [];
let rotated = 1;
let score = 0;
function createArray() {
    for (l = 0; l < 10; l++) {
        columns.push({ Type: "", State: "", Status: "", Space: "E" })
    }
    columns[0].Space = "Wall";
    columns[9].Space = "Wall";
    for (m = 0; m < 17; m++) {
        rows.push(columns.slice(0));
    }
    rows[0].Space = "floor";
    console.log(rows, "new");

}
createArray();

let paused = false;
let gameOver = false;
//Checks what key is pressed and calls a function accordingly
function keyCheck(event) {
    let keyPressed = event.keyCode;
    console.log(keyPressed);
    if (keyPressed == 37) {
        moveHorizontal(37);
    }
    else if (keyPressed == 39) {
        moveHorizontal(39);
    }
    else if (keyPressed == 40) {
        moveDown();
    }
    else if (keyPressed == 32) {
        pauseGame();
    }
    else if (keyPressed == 65) {
        rotateBlock(65);
    }
    else if (keyPressed == 49) {
        test(49);
    }
    else if (keyPressed == 50) {
        test(50);
    }
    else if (keyPressed == 51) {
        test(51);
    }
}
//Pauses Game if space is pressed
function pauseGame(event) {
    if (paused == false) {
        paused = true;
    }
    else if (paused == true && gameOver == false) {
        paused = false;
    }
}

//Rotates block if possible
function rotateBlock(event) {
    let getWidth = document.querySelector("rect:last-of-type").getAttribute("width");
    let getHeight = document.querySelector("rect:last-of-type").getAttribute("height");
    let getBlock = document.querySelector("rect:last-of-type");
    let getY = document.querySelector("rect:last-of-type").getAttribute("y");
    let Y = Number(getY);
    let width = Number(getWidth);
    let height = Number(getHeight);
    let sub;
    if (event == 65) {
        if (rotated == 1 && rows[currentRow - 1][currentColumn].Space == "E") {
            sub = width;
            width = height;
            height = sub;
            getBlock.setAttribute("width", width);
            getBlock.setAttribute("height", height);
            rotated += 1;

        }
        else if (rotated == 2 && rows[currentRow][currentColumn + 1].Space == "E") {
            sub = width;
            width = height;
            height = sub;
            getBlock.setAttribute("width", width);
            getBlock.setAttribute("height", height);
            rotated += 1;
        }
        if (rotated == 3) {
            rotated = 1;
        }
    }
}
//Moves the block left or right depending on which key was pressed
function moveHorizontal(event) {
    let getX = document.querySelector("rect:last-of-type").getAttribute("x");
    let X = Number(getX);
    if (paused == false) {
        if (blockList[0].size == "S") {
            if (event == 37 && X > 0 && rows[currentRow][currentColumn - 1].Space == "E") {
                X -= 50;
                getX = document.querySelector("rect:last-of-type")
                getX.setAttribute("x", X);
                currentColumn -= 1;
            }
            else if (event == 39 && X < 350 && rows[currentRow][currentColumn + 1].Space == "E") {
                X += 50;
                getX = document.querySelector("rect:last-of-type")
                getX.setAttribute("x", X);
                currentColumn += 1;
            }
        }
        else if (blockList[0].size == "M") {
            if (rotated == 1) {
                if (event == 37 && X > 0 && rows[currentRow][currentColumn - 1].Space == "E") {
                    X -= 50;
                    getX = document.querySelector("rect:last-of-type")
                    getX.setAttribute("x", X);
                    currentColumn -= 1;
                }
                else if (event == 39 && X < 300 && rows[currentRow][currentColumn + 2].Space == "E") {
                    X += 50;
                    getX = document.querySelector("rect:last-of-type")
                    getX.setAttribute("x", X);
                    currentColumn += 1;

                }
            }
            if (rotated == 2) {
                if (event == 37 && X > 0 && rows[currentRow][currentColumn - 1].Space == "E" && rows[currentRow - 1][currentColumn - 1].Space == "E") {
                    X -= 50;
                    getX = document.querySelector("rect:last-of-type")
                    getX.setAttribute("x", X);
                    currentColumn -= 1;
                }
                else if (event == 39 && X < 350 && rows[currentRow][currentColumn + 1].Space == "E" && rows[currentRow - 1][currentColumn + 1].Space == "E") {
                    X += 50;
                    getX = document.querySelector("rect:last-of-type")
                    getX.setAttribute("x", X);
                    currentColumn += 1;
                }
            }
        }
        else if (blockList[0].size == "L") {
            if (event == 37 && X > 0 && rows[currentRow][currentColumn - 1].Space == "E" && rows[currentRow - 1][currentColumn - 1].Space == "E") {
                X -= 50;
                getX = document.querySelector("rect:last-of-type")
                getX.setAttribute("x", X);
                currentColumn -= 1;
            }
            else if (event == 39 && X < 300 && rows[currentRow][currentColumn + 2].Space == "E" && rows[currentRow - 1][currentColumn + 1].Space == "E") {
                X += 50;
                getX = document.querySelector("rect:last-of-type")
                getX.setAttribute("x", X);
                currentColumn += 1;
            }
        }
    }
}
//Moves the block down if down is pressed
function moveDown(event) {
    let getY = document.querySelector("rect:last-of-type").getAttribute("y");
    let Y = Number(getY);
    if (paused == false) {
        if (blockList[0].size == "S" && Y < 750 && rows[currentRow - 1][currentColumn].Space == "E") {
            Y += 50;
            getY = document.querySelector("rect:last-of-type");
            getY.setAttribute("y", Y);
            currentRow -= 1;
        }
        else if (blockList[0].size == "M" && rotated == 1 && Y < 750 && blockList[0].state == "falling" && rows[currentRow - 1][currentColumn].Space == "E" && rows[currentRow - 1][currentColumn + 1].Space == "E") {
            Y += 50;
            getY = document.querySelector("rect:last-of-type");
            getY.setAttribute("y", Y);
            currentRow -= 1;
        }
        else if (blockList[0].size == "M" && rotated == 2 && Y < 700 && blockList[0].state == "falling" && rows[currentRow - 2][currentColumn].Space == "E") {
            Y += 50;
            getY = document.querySelector("rect:last-of-type");
            getY.setAttribute("y", Y);
            currentRow -= 1;
            console.log("stop");
        }
        else if (blockList[0].size == "L" && blockList[0].state == "falling" && Y < 700 && rows[currentRow - 2][currentColumn].Space == "E" && rows[currentRow - 2][currentColumn + 1].Space == "E") {
            Y += 50;
            getY = document.querySelector("rect:last-of-type");
            getY.setAttribute("y", Y);
            currentRow -= 1;
        }

    }
}

//Automatically moves block down and calls the block creation vfunction iff the space below is occupied
setInterval(function autoMoveDown() {
    if (paused == false) {
        let getY = document.querySelector("rect:last-of-type").getAttribute("y");
        let getHeight = document.querySelector("rect:last-of-type").getAttribute("height");
        let Y = Number(getY);

        if (blockList[0].size == "S" && Y < 750 && blockList[0].state == "falling" && rows[currentRow - 1][currentColumn].Space == "E") {
            Y += 50;
            getY = document.querySelector("rect:last-of-type");
            getY.setAttribute("y", Y);
            currentRow -= 1;
        }
        else if (blockList[0].size == "M" && rotated == 1 && Y < 750 && blockList[0].state == "falling" && rows[currentRow - 1][currentColumn].Space == "E" && rows[currentRow - 1][currentColumn + 1].Space == "E") {
            Y += 50;
            getY = document.querySelector("rect:last-of-type");
            getY.setAttribute("y", Y);
            currentRow -= 1;
        }
        else if (blockList[0].size == "M" && rotated == 2 && Y < 700 && blockList[0].state == "falling" && rows[currentRow - 2][currentColumn].Space == "E") {
            Y += 50;
            getY = document.querySelector("rect:last-of-type");
            getY.setAttribute("y", Y);
            currentRow -= 1;
            console.log("stop");
        }

        else if (blockList[0].size == "L" && blockList[0].state == "falling" && Y < 700 && rows[currentRow - 2][currentColumn].Space == "E" && rows[currentRow - 2][currentColumn + 1].Space == "E") {
            Y += 50;
            getY = document.querySelector("rect:last-of-type");
            getY.setAttribute("y", Y);
            currentRow -= 1;
        }
        else {
            
            updateArray();
            drawNewBlock();
        }
    }
}, 1000)

//Updates the 2d array
function updateArray() {
    let last = document.querySelector("rect:last-of-type");

    if (blockList[0].size == "S") {
        rows[currentRow][currentColumn] = { block: last, state: "stopped", size: "S", Space: "O" };
    }
    else if (blockList[0].size == "M") {
        if (rotated == 1) {
            rows[currentRow][currentColumn] = { block: last, state: "stopped", size: "M", Space: "O" };
            rows[currentRow][currentColumn + 1] = { block: last, state: "stopped", size: "M", Space: "O" };
        }
        else if (rotated == 2) {
            rows[currentRow][currentColumn] = { block: last, state: "stopped", size: "M", Space: "O" };
            rows[currentRow - 1][currentColumn] = { block: last, state: "stopped", size: "M", Space: "O" };
        }
        else if (rotated == 3) {
            rows[currentRow][currentColumn] = { block: last, state: "stopped", size: "M", Space: "O" };
            rows[currentRow][currentColumn - 1] = { block: last, state: "stopped", size: "M", Space: "O" };
        }
        else if (rotated == 4) {
            rows[currentRow][currentColumn] = { block: last, state: "stopped", size: "M", Space: "O" };
            rows[currentRow + 1][currentColumn] = { block: last, state: "stopped", size: "M", Space: "O" };
        }
    }
    else if (blockList[0].size == "L") {
        rows[currentRow][currentColumn] = { block: last, state: "stopped", size: "L", Space: "O" };
        rows[currentRow][currentColumn + 1] = { block: last, state: "stopped", size: "L", Space: "O" };
        rows[currentRow - 1][currentColumn] = { block: last, state: "stopped", size: "L", Space: "O" };
        rows[currentRow - 1][currentColumn + 1] = { block: last, state: "stopped", size: "L", Space: "O" };
    }
    for (let r = 0; r < 2; r++) {
        if (rows[r].Space == "O") {
            rows[r].Space == "E";
            rows[r].Type == "";
            rows.State = "falling";
        }
    }
    removeLines();
    endGame();
}

function removeLines() {
    let filledSpots = 0;
    let blockString = ''
    for (let r = 15; r > 0; r--) {
        for (let c = 0; c < 10; c++) {
            if (rows[r][c].Space == "O") {
                filledSpots += 1;
            
            if (filledSpots == 8) {
                rows.splice(r, 1);
                score += 1;
                console.log(rows);
                for (let r = 15; r > 0; r--) {
                    for (let b = 0; b < 10; c++) {
                        blockString += rows[r][b].block;
                    }
                }
                        console.log(blockString);
                        let output = document.querySelector("#output");
                        output.innerHTML = svgString + blockString;
                        console.log(output);
                    
                
            }
            else{
                console.log(filledSpots);
                filledSpots = 0;
            }
        }
            
        }

    }
}

function endGame() {
    for (o = 1; o < columns.length; o++) {
        if (rows[15][o].Space == "O") {
            gameOver = true;
            paused = true;
            break
        }
    }
}
//Draws new blocks
function drawNewBlock() {
    let output = document.querySelector("#output");
    let size = Math.floor((Math.random() * 3) + 1);
    if (size == 1) {
        let newBlockS = '<rect id="testBlock" x="0" y="0" width="50" height="50" stroke="black" fill="none"/>';
        output.innerHTML += newBlockS;
        blockList.pop();
        blockList.push({ block: newBlockS, state: "falling", size: "S" });
        currentColumn = 1;
        currentRow = 16;

    }
    else if (size == 2) {
        let newBlockM = '<rect id="testBlock" x="0" y="0" width="100" height="50" stroke="black" fill="none"/>';
        output.innerHTML += newBlockM;
        blockList.pop();
        blockList.push({ block: newBlockM, state: "falling", size: "M" });
        currentColumn = 1;
        currentRow = 16;
        rotated = 1;
    }
    else if (size == 3) {
        let newBlockL = '<rect id="testBlock" x="0" y="0" width="100" height="100" stroke="black" fill="none"/>';
        output.innerHTML += newBlockL;
        blockList.pop();
        blockList.push({ block: newBlockL, state: "falling", size: "L" });
        currentColumn = 1;
        currentRow = 16;
    }
}
///Test stuff doesn't actually work
function test(event){
    let output = document.querySelector("#output");
    if(event == 49){
        blockList.pop();
        blockList.push({ block: '<rect id="testBlockStop" x="0" y="0" width="50" height="50" stroke="black" fill="none"/>', state: "falling", size: "S" });
        output.innerHTML = svgString + '<rect id="testBlockStop" x="100" y="500" width="100" height="100" stroke="black" fill="none"/>' +
                  '<rect id="testBlockStop" x="100" y="600" width="100" height="100" stroke="black" fill="none"/>' +
                  '<rect id="testBlockStop" x="100" y="700" width="100" height="100" stroke="black" fill="none"/>' +
                  blockList[0].block;
                  console.log(output);
    }
    else if(event == 50){
        blockList.pop();
        blockList.push({ block: '<rect id="testBlockStop" x="0" y="0" width="50" height="100" stroke="black" fill="none"/>', state: "falling", size: "M" });
        output.innerHTML = svgString + '<rect id="testBlockStop" x="100" y="500" width="100" height="100" stroke="black" fill="none"/>' +
        '<rect id="testBlockStop" x="100" y="600" width="100" height="100" stroke="black" fill="none"/>' +
        '<rect id="testBlockStop" x="100" y="700" width="100" height="100" stroke="black" fill="none"/>' +
        blockList[0].block;
        console.log(output);

    }
    else if(event == 51){
        blockList.pop();
        blockList.push({ block: '<rect id="testBlockStop" x="0" y="0" width="100" height="100" stroke="black" fill="none"/>', state: "falling", size: "L" });
        output.innerHTML = svgString + '<rect id="testBlockStop" x="100" y="500" width="100" height="100" stroke="black" fill="none"/>' +
        '<rect id="testBlockStop" x="100" y="600" width="100" height="100" stroke="black" fill="none"/>' +
        '<rect id="testBlockStop" x="100" y="700" width="100" height="100" stroke="black" fill="none"/>' +
         blockList[0].block;
        console.log(output);
    }
   
}

window.keyCheck = keyCheck;

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtEQUEwQyxnQ0FBZ0M7QUFDMUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnRUFBd0Qsa0JBQWtCO0FBQzFFO0FBQ0EseURBQWlELGNBQWM7QUFDL0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEQUF5QyxpQ0FBaUM7QUFDMUUsd0hBQWdILG1CQUFtQixFQUFFO0FBQ3JJO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7OztBQUdBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQSxrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxRQUFRO0FBQ3ZCLHNCQUFzQiw4Q0FBOEM7QUFDcEU7QUFDQTtBQUNBO0FBQ0EsZUFBZSxRQUFRO0FBQ3ZCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsMkNBQTJDO0FBQzNDO0FBQ0E7QUFDQTtBQUNBLCtDQUErQztBQUMvQyxtREFBbUQ7QUFDbkQ7QUFDQTtBQUNBLCtDQUErQztBQUMvQyxtREFBbUQ7QUFDbkQ7QUFDQTtBQUNBLCtDQUErQztBQUMvQyxtREFBbUQ7QUFDbkQ7QUFDQTtBQUNBLCtDQUErQztBQUMvQyxtREFBbUQ7QUFDbkQ7QUFDQTtBQUNBO0FBQ0EsMkNBQTJDO0FBQzNDLCtDQUErQztBQUMvQywrQ0FBK0M7QUFDL0MsbURBQW1EO0FBQ25EO0FBQ0EsbUJBQW1CLE9BQU87QUFDMUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG9CQUFvQixPQUFPO0FBQzNCLHVCQUF1QixRQUFRO0FBQy9CO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnQ0FBZ0MsT0FBTztBQUN2QyxtQ0FBbUMsUUFBUTtBQUMzQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxlQUFlLG9CQUFvQjtBQUNuQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3QkFBd0IsZ0RBQWdEO0FBQ3hFO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdCQUF3QixnREFBZ0Q7QUFDeEU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdCQUF3QixnREFBZ0Q7QUFDeEU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0JBQXdCLGlJQUFpSTtBQUN6SjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0JBQXdCLGtJQUFrSTtBQUMxSjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLHdCQUF3QixtSUFBbUk7QUFDM0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBLDJCIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9pbmRleC5qc1wiKTtcbiIsImxldCBibG9ja0xpc3QgPSBbeyBibG9jazogJzxyZWN0IGlkPVwidGVzdEJsb2NrXCIgeD1cIjBcIiB5PVwiMFwiIHdpZHRoPVwiNTBcIiBoZWlnaHQ9XCI1MFwiIHN0cm9rZT1cImJsYWNrXCIgZmlsbD1cIm5vbmVcIi8+Jywgc3RhdGU6IFwiZmFsbGluZ1wiLCBzaXplOiBcIlNcIiB9XHJcbl07XHJcbmxldCBzdmdTdHJpbmcgPSAnPHJlY3Qgd2lkdGg9XCI0MDBcIiBoZWlnaHQ9XCI4MDBcIiBzdHJva2U9XCJibGFja1wiIGZpbGw9XCJub25lXCIvPic7XHJcbmxldCBjdXJyZW50Q29sdW1uID0gMTtcclxubGV0IGN1cnJlbnRSb3cgPSAxNjtcclxubGV0IGNvbHVtbnMgPSBbXTtcclxubGV0IHJvd3MgPSBbXTtcclxubGV0IHJvdGF0ZWQgPSAxO1xyXG5sZXQgc2NvcmUgPSAwO1xyXG5mdW5jdGlvbiBjcmVhdGVBcnJheSgpIHtcclxuICAgIGZvciAobCA9IDA7IGwgPCAxMDsgbCsrKSB7XHJcbiAgICAgICAgY29sdW1ucy5wdXNoKHsgVHlwZTogXCJcIiwgU3RhdGU6IFwiXCIsIFN0YXR1czogXCJcIiwgU3BhY2U6IFwiRVwiIH0pXHJcbiAgICB9XHJcbiAgICBjb2x1bW5zWzBdLlNwYWNlID0gXCJXYWxsXCI7XHJcbiAgICBjb2x1bW5zWzldLlNwYWNlID0gXCJXYWxsXCI7XHJcbiAgICBmb3IgKG0gPSAwOyBtIDwgMTc7IG0rKykge1xyXG4gICAgICAgIHJvd3MucHVzaChjb2x1bW5zLnNsaWNlKDApKTtcclxuICAgIH1cclxuICAgIHJvd3NbMF0uU3BhY2UgPSBcImZsb29yXCI7XHJcbiAgICBjb25zb2xlLmxvZyhyb3dzLCBcIm5ld1wiKTtcclxuXHJcbn1cclxuY3JlYXRlQXJyYXkoKTtcclxuXHJcbmxldCBwYXVzZWQgPSBmYWxzZTtcclxubGV0IGdhbWVPdmVyID0gZmFsc2U7XHJcbi8vQ2hlY2tzIHdoYXQga2V5IGlzIHByZXNzZWQgYW5kIGNhbGxzIGEgZnVuY3Rpb24gYWNjb3JkaW5nbHlcclxuZnVuY3Rpb24ga2V5Q2hlY2soZXZlbnQpIHtcclxuICAgIGxldCBrZXlQcmVzc2VkID0gZXZlbnQua2V5Q29kZTtcclxuICAgIGNvbnNvbGUubG9nKGtleVByZXNzZWQpO1xyXG4gICAgaWYgKGtleVByZXNzZWQgPT0gMzcpIHtcclxuICAgICAgICBtb3ZlSG9yaXpvbnRhbCgzNyk7XHJcbiAgICB9XHJcbiAgICBlbHNlIGlmIChrZXlQcmVzc2VkID09IDM5KSB7XHJcbiAgICAgICAgbW92ZUhvcml6b250YWwoMzkpO1xyXG4gICAgfVxyXG4gICAgZWxzZSBpZiAoa2V5UHJlc3NlZCA9PSA0MCkge1xyXG4gICAgICAgIG1vdmVEb3duKCk7XHJcbiAgICB9XHJcbiAgICBlbHNlIGlmIChrZXlQcmVzc2VkID09IDMyKSB7XHJcbiAgICAgICAgcGF1c2VHYW1lKCk7XHJcbiAgICB9XHJcbiAgICBlbHNlIGlmIChrZXlQcmVzc2VkID09IDY1KSB7XHJcbiAgICAgICAgcm90YXRlQmxvY2soNjUpO1xyXG4gICAgfVxyXG4gICAgZWxzZSBpZiAoa2V5UHJlc3NlZCA9PSA0OSkge1xyXG4gICAgICAgIHRlc3QoNDkpO1xyXG4gICAgfVxyXG4gICAgZWxzZSBpZiAoa2V5UHJlc3NlZCA9PSA1MCkge1xyXG4gICAgICAgIHRlc3QoNTApO1xyXG4gICAgfVxyXG4gICAgZWxzZSBpZiAoa2V5UHJlc3NlZCA9PSA1MSkge1xyXG4gICAgICAgIHRlc3QoNTEpO1xyXG4gICAgfVxyXG59XHJcbi8vUGF1c2VzIEdhbWUgaWYgc3BhY2UgaXMgcHJlc3NlZFxyXG5mdW5jdGlvbiBwYXVzZUdhbWUoZXZlbnQpIHtcclxuICAgIGlmIChwYXVzZWQgPT0gZmFsc2UpIHtcclxuICAgICAgICBwYXVzZWQgPSB0cnVlO1xyXG4gICAgfVxyXG4gICAgZWxzZSBpZiAocGF1c2VkID09IHRydWUgJiYgZ2FtZU92ZXIgPT0gZmFsc2UpIHtcclxuICAgICAgICBwYXVzZWQgPSBmYWxzZTtcclxuICAgIH1cclxufVxyXG5cclxuLy9Sb3RhdGVzIGJsb2NrIGlmIHBvc3NpYmxlXHJcbmZ1bmN0aW9uIHJvdGF0ZUJsb2NrKGV2ZW50KSB7XHJcbiAgICBsZXQgZ2V0V2lkdGggPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwicmVjdDpsYXN0LW9mLXR5cGVcIikuZ2V0QXR0cmlidXRlKFwid2lkdGhcIik7XHJcbiAgICBsZXQgZ2V0SGVpZ2h0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcInJlY3Q6bGFzdC1vZi10eXBlXCIpLmdldEF0dHJpYnV0ZShcImhlaWdodFwiKTtcclxuICAgIGxldCBnZXRCbG9jayA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJyZWN0Omxhc3Qtb2YtdHlwZVwiKTtcclxuICAgIGxldCBnZXRZID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcInJlY3Q6bGFzdC1vZi10eXBlXCIpLmdldEF0dHJpYnV0ZShcInlcIik7XHJcbiAgICBsZXQgWSA9IE51bWJlcihnZXRZKTtcclxuICAgIGxldCB3aWR0aCA9IE51bWJlcihnZXRXaWR0aCk7XHJcbiAgICBsZXQgaGVpZ2h0ID0gTnVtYmVyKGdldEhlaWdodCk7XHJcbiAgICBsZXQgc3ViO1xyXG4gICAgaWYgKGV2ZW50ID09IDY1KSB7XHJcbiAgICAgICAgaWYgKHJvdGF0ZWQgPT0gMSAmJiByb3dzW2N1cnJlbnRSb3cgLSAxXVtjdXJyZW50Q29sdW1uXS5TcGFjZSA9PSBcIkVcIikge1xyXG4gICAgICAgICAgICBzdWIgPSB3aWR0aDtcclxuICAgICAgICAgICAgd2lkdGggPSBoZWlnaHQ7XHJcbiAgICAgICAgICAgIGhlaWdodCA9IHN1YjtcclxuICAgICAgICAgICAgZ2V0QmxvY2suc2V0QXR0cmlidXRlKFwid2lkdGhcIiwgd2lkdGgpO1xyXG4gICAgICAgICAgICBnZXRCbG9jay5zZXRBdHRyaWJ1dGUoXCJoZWlnaHRcIiwgaGVpZ2h0KTtcclxuICAgICAgICAgICAgcm90YXRlZCArPSAxO1xyXG5cclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSBpZiAocm90YXRlZCA9PSAyICYmIHJvd3NbY3VycmVudFJvd11bY3VycmVudENvbHVtbiArIDFdLlNwYWNlID09IFwiRVwiKSB7XHJcbiAgICAgICAgICAgIHN1YiA9IHdpZHRoO1xyXG4gICAgICAgICAgICB3aWR0aCA9IGhlaWdodDtcclxuICAgICAgICAgICAgaGVpZ2h0ID0gc3ViO1xyXG4gICAgICAgICAgICBnZXRCbG9jay5zZXRBdHRyaWJ1dGUoXCJ3aWR0aFwiLCB3aWR0aCk7XHJcbiAgICAgICAgICAgIGdldEJsb2NrLnNldEF0dHJpYnV0ZShcImhlaWdodFwiLCBoZWlnaHQpO1xyXG4gICAgICAgICAgICByb3RhdGVkICs9IDE7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChyb3RhdGVkID09IDMpIHtcclxuICAgICAgICAgICAgcm90YXRlZCA9IDE7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbi8vTW92ZXMgdGhlIGJsb2NrIGxlZnQgb3IgcmlnaHQgZGVwZW5kaW5nIG9uIHdoaWNoIGtleSB3YXMgcHJlc3NlZFxyXG5mdW5jdGlvbiBtb3ZlSG9yaXpvbnRhbChldmVudCkge1xyXG4gICAgbGV0IGdldFggPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwicmVjdDpsYXN0LW9mLXR5cGVcIikuZ2V0QXR0cmlidXRlKFwieFwiKTtcclxuICAgIGxldCBYID0gTnVtYmVyKGdldFgpO1xyXG4gICAgaWYgKHBhdXNlZCA9PSBmYWxzZSkge1xyXG4gICAgICAgIGlmIChibG9ja0xpc3RbMF0uc2l6ZSA9PSBcIlNcIikge1xyXG4gICAgICAgICAgICBpZiAoZXZlbnQgPT0gMzcgJiYgWCA+IDAgJiYgcm93c1tjdXJyZW50Um93XVtjdXJyZW50Q29sdW1uIC0gMV0uU3BhY2UgPT0gXCJFXCIpIHtcclxuICAgICAgICAgICAgICAgIFggLT0gNTA7XHJcbiAgICAgICAgICAgICAgICBnZXRYID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcInJlY3Q6bGFzdC1vZi10eXBlXCIpXHJcbiAgICAgICAgICAgICAgICBnZXRYLnNldEF0dHJpYnV0ZShcInhcIiwgWCk7XHJcbiAgICAgICAgICAgICAgICBjdXJyZW50Q29sdW1uIC09IDE7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSBpZiAoZXZlbnQgPT0gMzkgJiYgWCA8IDM1MCAmJiByb3dzW2N1cnJlbnRSb3ddW2N1cnJlbnRDb2x1bW4gKyAxXS5TcGFjZSA9PSBcIkVcIikge1xyXG4gICAgICAgICAgICAgICAgWCArPSA1MDtcclxuICAgICAgICAgICAgICAgIGdldFggPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwicmVjdDpsYXN0LW9mLXR5cGVcIilcclxuICAgICAgICAgICAgICAgIGdldFguc2V0QXR0cmlidXRlKFwieFwiLCBYKTtcclxuICAgICAgICAgICAgICAgIGN1cnJlbnRDb2x1bW4gKz0gMTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIGlmIChibG9ja0xpc3RbMF0uc2l6ZSA9PSBcIk1cIikge1xyXG4gICAgICAgICAgICBpZiAocm90YXRlZCA9PSAxKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZXZlbnQgPT0gMzcgJiYgWCA+IDAgJiYgcm93c1tjdXJyZW50Um93XVtjdXJyZW50Q29sdW1uIC0gMV0uU3BhY2UgPT0gXCJFXCIpIHtcclxuICAgICAgICAgICAgICAgICAgICBYIC09IDUwO1xyXG4gICAgICAgICAgICAgICAgICAgIGdldFggPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwicmVjdDpsYXN0LW9mLXR5cGVcIilcclxuICAgICAgICAgICAgICAgICAgICBnZXRYLnNldEF0dHJpYnV0ZShcInhcIiwgWCk7XHJcbiAgICAgICAgICAgICAgICAgICAgY3VycmVudENvbHVtbiAtPSAxO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZWxzZSBpZiAoZXZlbnQgPT0gMzkgJiYgWCA8IDMwMCAmJiByb3dzW2N1cnJlbnRSb3ddW2N1cnJlbnRDb2x1bW4gKyAyXS5TcGFjZSA9PSBcIkVcIikge1xyXG4gICAgICAgICAgICAgICAgICAgIFggKz0gNTA7XHJcbiAgICAgICAgICAgICAgICAgICAgZ2V0WCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJyZWN0Omxhc3Qtb2YtdHlwZVwiKVxyXG4gICAgICAgICAgICAgICAgICAgIGdldFguc2V0QXR0cmlidXRlKFwieFwiLCBYKTtcclxuICAgICAgICAgICAgICAgICAgICBjdXJyZW50Q29sdW1uICs9IDE7XHJcblxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChyb3RhdGVkID09IDIpIHtcclxuICAgICAgICAgICAgICAgIGlmIChldmVudCA9PSAzNyAmJiBYID4gMCAmJiByb3dzW2N1cnJlbnRSb3ddW2N1cnJlbnRDb2x1bW4gLSAxXS5TcGFjZSA9PSBcIkVcIiAmJiByb3dzW2N1cnJlbnRSb3cgLSAxXVtjdXJyZW50Q29sdW1uIC0gMV0uU3BhY2UgPT0gXCJFXCIpIHtcclxuICAgICAgICAgICAgICAgICAgICBYIC09IDUwO1xyXG4gICAgICAgICAgICAgICAgICAgIGdldFggPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwicmVjdDpsYXN0LW9mLXR5cGVcIilcclxuICAgICAgICAgICAgICAgICAgICBnZXRYLnNldEF0dHJpYnV0ZShcInhcIiwgWCk7XHJcbiAgICAgICAgICAgICAgICAgICAgY3VycmVudENvbHVtbiAtPSAxO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZWxzZSBpZiAoZXZlbnQgPT0gMzkgJiYgWCA8IDM1MCAmJiByb3dzW2N1cnJlbnRSb3ddW2N1cnJlbnRDb2x1bW4gKyAxXS5TcGFjZSA9PSBcIkVcIiAmJiByb3dzW2N1cnJlbnRSb3cgLSAxXVtjdXJyZW50Q29sdW1uICsgMV0uU3BhY2UgPT0gXCJFXCIpIHtcclxuICAgICAgICAgICAgICAgICAgICBYICs9IDUwO1xyXG4gICAgICAgICAgICAgICAgICAgIGdldFggPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwicmVjdDpsYXN0LW9mLXR5cGVcIilcclxuICAgICAgICAgICAgICAgICAgICBnZXRYLnNldEF0dHJpYnV0ZShcInhcIiwgWCk7XHJcbiAgICAgICAgICAgICAgICAgICAgY3VycmVudENvbHVtbiArPSAxO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2UgaWYgKGJsb2NrTGlzdFswXS5zaXplID09IFwiTFwiKSB7XHJcbiAgICAgICAgICAgIGlmIChldmVudCA9PSAzNyAmJiBYID4gMCAmJiByb3dzW2N1cnJlbnRSb3ddW2N1cnJlbnRDb2x1bW4gLSAxXS5TcGFjZSA9PSBcIkVcIiAmJiByb3dzW2N1cnJlbnRSb3cgLSAxXVtjdXJyZW50Q29sdW1uIC0gMV0uU3BhY2UgPT0gXCJFXCIpIHtcclxuICAgICAgICAgICAgICAgIFggLT0gNTA7XHJcbiAgICAgICAgICAgICAgICBnZXRYID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcInJlY3Q6bGFzdC1vZi10eXBlXCIpXHJcbiAgICAgICAgICAgICAgICBnZXRYLnNldEF0dHJpYnV0ZShcInhcIiwgWCk7XHJcbiAgICAgICAgICAgICAgICBjdXJyZW50Q29sdW1uIC09IDE7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSBpZiAoZXZlbnQgPT0gMzkgJiYgWCA8IDMwMCAmJiByb3dzW2N1cnJlbnRSb3ddW2N1cnJlbnRDb2x1bW4gKyAyXS5TcGFjZSA9PSBcIkVcIiAmJiByb3dzW2N1cnJlbnRSb3cgLSAxXVtjdXJyZW50Q29sdW1uICsgMV0uU3BhY2UgPT0gXCJFXCIpIHtcclxuICAgICAgICAgICAgICAgIFggKz0gNTA7XHJcbiAgICAgICAgICAgICAgICBnZXRYID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcInJlY3Q6bGFzdC1vZi10eXBlXCIpXHJcbiAgICAgICAgICAgICAgICBnZXRYLnNldEF0dHJpYnV0ZShcInhcIiwgWCk7XHJcbiAgICAgICAgICAgICAgICBjdXJyZW50Q29sdW1uICs9IDE7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuLy9Nb3ZlcyB0aGUgYmxvY2sgZG93biBpZiBkb3duIGlzIHByZXNzZWRcclxuZnVuY3Rpb24gbW92ZURvd24oZXZlbnQpIHtcclxuICAgIGxldCBnZXRZID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcInJlY3Q6bGFzdC1vZi10eXBlXCIpLmdldEF0dHJpYnV0ZShcInlcIik7XHJcbiAgICBsZXQgWSA9IE51bWJlcihnZXRZKTtcclxuICAgIGlmIChwYXVzZWQgPT0gZmFsc2UpIHtcclxuICAgICAgICBpZiAoYmxvY2tMaXN0WzBdLnNpemUgPT0gXCJTXCIgJiYgWSA8IDc1MCAmJiByb3dzW2N1cnJlbnRSb3cgLSAxXVtjdXJyZW50Q29sdW1uXS5TcGFjZSA9PSBcIkVcIikge1xyXG4gICAgICAgICAgICBZICs9IDUwO1xyXG4gICAgICAgICAgICBnZXRZID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcInJlY3Q6bGFzdC1vZi10eXBlXCIpO1xyXG4gICAgICAgICAgICBnZXRZLnNldEF0dHJpYnV0ZShcInlcIiwgWSk7XHJcbiAgICAgICAgICAgIGN1cnJlbnRSb3cgLT0gMTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSBpZiAoYmxvY2tMaXN0WzBdLnNpemUgPT0gXCJNXCIgJiYgcm90YXRlZCA9PSAxICYmIFkgPCA3NTAgJiYgYmxvY2tMaXN0WzBdLnN0YXRlID09IFwiZmFsbGluZ1wiICYmIHJvd3NbY3VycmVudFJvdyAtIDFdW2N1cnJlbnRDb2x1bW5dLlNwYWNlID09IFwiRVwiICYmIHJvd3NbY3VycmVudFJvdyAtIDFdW2N1cnJlbnRDb2x1bW4gKyAxXS5TcGFjZSA9PSBcIkVcIikge1xyXG4gICAgICAgICAgICBZICs9IDUwO1xyXG4gICAgICAgICAgICBnZXRZID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcInJlY3Q6bGFzdC1vZi10eXBlXCIpO1xyXG4gICAgICAgICAgICBnZXRZLnNldEF0dHJpYnV0ZShcInlcIiwgWSk7XHJcbiAgICAgICAgICAgIGN1cnJlbnRSb3cgLT0gMTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSBpZiAoYmxvY2tMaXN0WzBdLnNpemUgPT0gXCJNXCIgJiYgcm90YXRlZCA9PSAyICYmIFkgPCA3MDAgJiYgYmxvY2tMaXN0WzBdLnN0YXRlID09IFwiZmFsbGluZ1wiICYmIHJvd3NbY3VycmVudFJvdyAtIDJdW2N1cnJlbnRDb2x1bW5dLlNwYWNlID09IFwiRVwiKSB7XHJcbiAgICAgICAgICAgIFkgKz0gNTA7XHJcbiAgICAgICAgICAgIGdldFkgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwicmVjdDpsYXN0LW9mLXR5cGVcIik7XHJcbiAgICAgICAgICAgIGdldFkuc2V0QXR0cmlidXRlKFwieVwiLCBZKTtcclxuICAgICAgICAgICAgY3VycmVudFJvdyAtPSAxO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcInN0b3BcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2UgaWYgKGJsb2NrTGlzdFswXS5zaXplID09IFwiTFwiICYmIGJsb2NrTGlzdFswXS5zdGF0ZSA9PSBcImZhbGxpbmdcIiAmJiBZIDwgNzAwICYmIHJvd3NbY3VycmVudFJvdyAtIDJdW2N1cnJlbnRDb2x1bW5dLlNwYWNlID09IFwiRVwiICYmIHJvd3NbY3VycmVudFJvdyAtIDJdW2N1cnJlbnRDb2x1bW4gKyAxXS5TcGFjZSA9PSBcIkVcIikge1xyXG4gICAgICAgICAgICBZICs9IDUwO1xyXG4gICAgICAgICAgICBnZXRZID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcInJlY3Q6bGFzdC1vZi10eXBlXCIpO1xyXG4gICAgICAgICAgICBnZXRZLnNldEF0dHJpYnV0ZShcInlcIiwgWSk7XHJcbiAgICAgICAgICAgIGN1cnJlbnRSb3cgLT0gMTtcclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG59XHJcblxyXG4vL0F1dG9tYXRpY2FsbHkgbW92ZXMgYmxvY2sgZG93biBhbmQgY2FsbHMgdGhlIGJsb2NrIGNyZWF0aW9uIHZmdW5jdGlvbiBpZmYgdGhlIHNwYWNlIGJlbG93IGlzIG9jY3VwaWVkXHJcbnNldEludGVydmFsKGZ1bmN0aW9uIGF1dG9Nb3ZlRG93bigpIHtcclxuICAgIGlmIChwYXVzZWQgPT0gZmFsc2UpIHtcclxuICAgICAgICBsZXQgZ2V0WSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJyZWN0Omxhc3Qtb2YtdHlwZVwiKS5nZXRBdHRyaWJ1dGUoXCJ5XCIpO1xyXG4gICAgICAgIGxldCBnZXRIZWlnaHQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwicmVjdDpsYXN0LW9mLXR5cGVcIikuZ2V0QXR0cmlidXRlKFwiaGVpZ2h0XCIpO1xyXG4gICAgICAgIGxldCBZID0gTnVtYmVyKGdldFkpO1xyXG5cclxuICAgICAgICBpZiAoYmxvY2tMaXN0WzBdLnNpemUgPT0gXCJTXCIgJiYgWSA8IDc1MCAmJiBibG9ja0xpc3RbMF0uc3RhdGUgPT0gXCJmYWxsaW5nXCIgJiYgcm93c1tjdXJyZW50Um93IC0gMV1bY3VycmVudENvbHVtbl0uU3BhY2UgPT0gXCJFXCIpIHtcclxuICAgICAgICAgICAgWSArPSA1MDtcclxuICAgICAgICAgICAgZ2V0WSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJyZWN0Omxhc3Qtb2YtdHlwZVwiKTtcclxuICAgICAgICAgICAgZ2V0WS5zZXRBdHRyaWJ1dGUoXCJ5XCIsIFkpO1xyXG4gICAgICAgICAgICBjdXJyZW50Um93IC09IDE7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2UgaWYgKGJsb2NrTGlzdFswXS5zaXplID09IFwiTVwiICYmIHJvdGF0ZWQgPT0gMSAmJiBZIDwgNzUwICYmIGJsb2NrTGlzdFswXS5zdGF0ZSA9PSBcImZhbGxpbmdcIiAmJiByb3dzW2N1cnJlbnRSb3cgLSAxXVtjdXJyZW50Q29sdW1uXS5TcGFjZSA9PSBcIkVcIiAmJiByb3dzW2N1cnJlbnRSb3cgLSAxXVtjdXJyZW50Q29sdW1uICsgMV0uU3BhY2UgPT0gXCJFXCIpIHtcclxuICAgICAgICAgICAgWSArPSA1MDtcclxuICAgICAgICAgICAgZ2V0WSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJyZWN0Omxhc3Qtb2YtdHlwZVwiKTtcclxuICAgICAgICAgICAgZ2V0WS5zZXRBdHRyaWJ1dGUoXCJ5XCIsIFkpO1xyXG4gICAgICAgICAgICBjdXJyZW50Um93IC09IDE7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2UgaWYgKGJsb2NrTGlzdFswXS5zaXplID09IFwiTVwiICYmIHJvdGF0ZWQgPT0gMiAmJiBZIDwgNzAwICYmIGJsb2NrTGlzdFswXS5zdGF0ZSA9PSBcImZhbGxpbmdcIiAmJiByb3dzW2N1cnJlbnRSb3cgLSAyXVtjdXJyZW50Q29sdW1uXS5TcGFjZSA9PSBcIkVcIikge1xyXG4gICAgICAgICAgICBZICs9IDUwO1xyXG4gICAgICAgICAgICBnZXRZID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcInJlY3Q6bGFzdC1vZi10eXBlXCIpO1xyXG4gICAgICAgICAgICBnZXRZLnNldEF0dHJpYnV0ZShcInlcIiwgWSk7XHJcbiAgICAgICAgICAgIGN1cnJlbnRSb3cgLT0gMTtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJzdG9wXCIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZWxzZSBpZiAoYmxvY2tMaXN0WzBdLnNpemUgPT0gXCJMXCIgJiYgYmxvY2tMaXN0WzBdLnN0YXRlID09IFwiZmFsbGluZ1wiICYmIFkgPCA3MDAgJiYgcm93c1tjdXJyZW50Um93IC0gMl1bY3VycmVudENvbHVtbl0uU3BhY2UgPT0gXCJFXCIgJiYgcm93c1tjdXJyZW50Um93IC0gMl1bY3VycmVudENvbHVtbiArIDFdLlNwYWNlID09IFwiRVwiKSB7XHJcbiAgICAgICAgICAgIFkgKz0gNTA7XHJcbiAgICAgICAgICAgIGdldFkgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwicmVjdDpsYXN0LW9mLXR5cGVcIik7XHJcbiAgICAgICAgICAgIGdldFkuc2V0QXR0cmlidXRlKFwieVwiLCBZKTtcclxuICAgICAgICAgICAgY3VycmVudFJvdyAtPSAxO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHVwZGF0ZUFycmF5KCk7XHJcbiAgICAgICAgICAgIGRyYXdOZXdCbG9jaygpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufSwgMTAwMClcclxuXHJcbi8vVXBkYXRlcyB0aGUgMmQgYXJyYXlcclxuZnVuY3Rpb24gdXBkYXRlQXJyYXkoKSB7XHJcbiAgICBsZXQgbGFzdCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJyZWN0Omxhc3Qtb2YtdHlwZVwiKTtcclxuXHJcbiAgICBpZiAoYmxvY2tMaXN0WzBdLnNpemUgPT0gXCJTXCIpIHtcclxuICAgICAgICByb3dzW2N1cnJlbnRSb3ddW2N1cnJlbnRDb2x1bW5dID0geyBibG9jazogbGFzdCwgc3RhdGU6IFwic3RvcHBlZFwiLCBzaXplOiBcIlNcIiwgU3BhY2U6IFwiT1wiIH07XHJcbiAgICB9XHJcbiAgICBlbHNlIGlmIChibG9ja0xpc3RbMF0uc2l6ZSA9PSBcIk1cIikge1xyXG4gICAgICAgIGlmIChyb3RhdGVkID09IDEpIHtcclxuICAgICAgICAgICAgcm93c1tjdXJyZW50Um93XVtjdXJyZW50Q29sdW1uXSA9IHsgYmxvY2s6IGxhc3QsIHN0YXRlOiBcInN0b3BwZWRcIiwgc2l6ZTogXCJNXCIsIFNwYWNlOiBcIk9cIiB9O1xyXG4gICAgICAgICAgICByb3dzW2N1cnJlbnRSb3ddW2N1cnJlbnRDb2x1bW4gKyAxXSA9IHsgYmxvY2s6IGxhc3QsIHN0YXRlOiBcInN0b3BwZWRcIiwgc2l6ZTogXCJNXCIsIFNwYWNlOiBcIk9cIiB9O1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIGlmIChyb3RhdGVkID09IDIpIHtcclxuICAgICAgICAgICAgcm93c1tjdXJyZW50Um93XVtjdXJyZW50Q29sdW1uXSA9IHsgYmxvY2s6IGxhc3QsIHN0YXRlOiBcInN0b3BwZWRcIiwgc2l6ZTogXCJNXCIsIFNwYWNlOiBcIk9cIiB9O1xyXG4gICAgICAgICAgICByb3dzW2N1cnJlbnRSb3cgLSAxXVtjdXJyZW50Q29sdW1uXSA9IHsgYmxvY2s6IGxhc3QsIHN0YXRlOiBcInN0b3BwZWRcIiwgc2l6ZTogXCJNXCIsIFNwYWNlOiBcIk9cIiB9O1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIGlmIChyb3RhdGVkID09IDMpIHtcclxuICAgICAgICAgICAgcm93c1tjdXJyZW50Um93XVtjdXJyZW50Q29sdW1uXSA9IHsgYmxvY2s6IGxhc3QsIHN0YXRlOiBcInN0b3BwZWRcIiwgc2l6ZTogXCJNXCIsIFNwYWNlOiBcIk9cIiB9O1xyXG4gICAgICAgICAgICByb3dzW2N1cnJlbnRSb3ddW2N1cnJlbnRDb2x1bW4gLSAxXSA9IHsgYmxvY2s6IGxhc3QsIHN0YXRlOiBcInN0b3BwZWRcIiwgc2l6ZTogXCJNXCIsIFNwYWNlOiBcIk9cIiB9O1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIGlmIChyb3RhdGVkID09IDQpIHtcclxuICAgICAgICAgICAgcm93c1tjdXJyZW50Um93XVtjdXJyZW50Q29sdW1uXSA9IHsgYmxvY2s6IGxhc3QsIHN0YXRlOiBcInN0b3BwZWRcIiwgc2l6ZTogXCJNXCIsIFNwYWNlOiBcIk9cIiB9O1xyXG4gICAgICAgICAgICByb3dzW2N1cnJlbnRSb3cgKyAxXVtjdXJyZW50Q29sdW1uXSA9IHsgYmxvY2s6IGxhc3QsIHN0YXRlOiBcInN0b3BwZWRcIiwgc2l6ZTogXCJNXCIsIFNwYWNlOiBcIk9cIiB9O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIGVsc2UgaWYgKGJsb2NrTGlzdFswXS5zaXplID09IFwiTFwiKSB7XHJcbiAgICAgICAgcm93c1tjdXJyZW50Um93XVtjdXJyZW50Q29sdW1uXSA9IHsgYmxvY2s6IGxhc3QsIHN0YXRlOiBcInN0b3BwZWRcIiwgc2l6ZTogXCJMXCIsIFNwYWNlOiBcIk9cIiB9O1xyXG4gICAgICAgIHJvd3NbY3VycmVudFJvd11bY3VycmVudENvbHVtbiArIDFdID0geyBibG9jazogbGFzdCwgc3RhdGU6IFwic3RvcHBlZFwiLCBzaXplOiBcIkxcIiwgU3BhY2U6IFwiT1wiIH07XHJcbiAgICAgICAgcm93c1tjdXJyZW50Um93IC0gMV1bY3VycmVudENvbHVtbl0gPSB7IGJsb2NrOiBsYXN0LCBzdGF0ZTogXCJzdG9wcGVkXCIsIHNpemU6IFwiTFwiLCBTcGFjZTogXCJPXCIgfTtcclxuICAgICAgICByb3dzW2N1cnJlbnRSb3cgLSAxXVtjdXJyZW50Q29sdW1uICsgMV0gPSB7IGJsb2NrOiBsYXN0LCBzdGF0ZTogXCJzdG9wcGVkXCIsIHNpemU6IFwiTFwiLCBTcGFjZTogXCJPXCIgfTtcclxuICAgIH1cclxuICAgIGZvciAobGV0IHIgPSAwOyByIDwgMjsgcisrKSB7XHJcbiAgICAgICAgaWYgKHJvd3Nbcl0uU3BhY2UgPT0gXCJPXCIpIHtcclxuICAgICAgICAgICAgcm93c1tyXS5TcGFjZSA9PSBcIkVcIjtcclxuICAgICAgICAgICAgcm93c1tyXS5UeXBlID09IFwiXCI7XHJcbiAgICAgICAgICAgIHJvd3MuU3RhdGUgPSBcImZhbGxpbmdcIjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICByZW1vdmVMaW5lcygpO1xyXG4gICAgZW5kR2FtZSgpO1xyXG59XHJcblxyXG5mdW5jdGlvbiByZW1vdmVMaW5lcygpIHtcclxuICAgIGxldCBmaWxsZWRTcG90cyA9IDA7XHJcbiAgICBsZXQgYmxvY2tTdHJpbmcgPSAnJ1xyXG4gICAgZm9yIChsZXQgciA9IDE1OyByID4gMDsgci0tKSB7XHJcbiAgICAgICAgZm9yIChsZXQgYyA9IDA7IGMgPCAxMDsgYysrKSB7XHJcbiAgICAgICAgICAgIGlmIChyb3dzW3JdW2NdLlNwYWNlID09IFwiT1wiKSB7XHJcbiAgICAgICAgICAgICAgICBmaWxsZWRTcG90cyArPSAxO1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgaWYgKGZpbGxlZFNwb3RzID09IDgpIHtcclxuICAgICAgICAgICAgICAgIHJvd3Muc3BsaWNlKHIsIDEpO1xyXG4gICAgICAgICAgICAgICAgc2NvcmUgKz0gMTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJvd3MpO1xyXG4gICAgICAgICAgICAgICAgZm9yIChsZXQgciA9IDE1OyByID4gMDsgci0tKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9yIChsZXQgYiA9IDA7IGIgPCAxMDsgYysrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJsb2NrU3RyaW5nICs9IHJvd3Nbcl1bYl0uYmxvY2s7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhibG9ja1N0cmluZyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCBvdXRwdXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI291dHB1dFwiKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3V0cHV0LmlubmVySFRNTCA9IHN2Z1N0cmluZyArIGJsb2NrU3RyaW5nO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhvdXRwdXQpO1xyXG4gICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGZpbGxlZFNwb3RzKTtcclxuICAgICAgICAgICAgICAgIGZpbGxlZFNwb3RzID0gMDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAgICAgXHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxufVxyXG5cclxuZnVuY3Rpb24gZW5kR2FtZSgpIHtcclxuICAgIGZvciAobyA9IDE7IG8gPCBjb2x1bW5zLmxlbmd0aDsgbysrKSB7XHJcbiAgICAgICAgaWYgKHJvd3NbMTVdW29dLlNwYWNlID09IFwiT1wiKSB7XHJcbiAgICAgICAgICAgIGdhbWVPdmVyID0gdHJ1ZTtcclxuICAgICAgICAgICAgcGF1c2VkID0gdHJ1ZTtcclxuICAgICAgICAgICAgYnJlYWtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuLy9EcmF3cyBuZXcgYmxvY2tzXHJcbmZ1bmN0aW9uIGRyYXdOZXdCbG9jaygpIHtcclxuICAgIGxldCBvdXRwdXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI291dHB1dFwiKTtcclxuICAgIGxldCBzaXplID0gTWF0aC5mbG9vcigoTWF0aC5yYW5kb20oKSAqIDMpICsgMSk7XHJcbiAgICBpZiAoc2l6ZSA9PSAxKSB7XHJcbiAgICAgICAgbGV0IG5ld0Jsb2NrUyA9ICc8cmVjdCBpZD1cInRlc3RCbG9ja1wiIHg9XCIwXCIgeT1cIjBcIiB3aWR0aD1cIjUwXCIgaGVpZ2h0PVwiNTBcIiBzdHJva2U9XCJibGFja1wiIGZpbGw9XCJub25lXCIvPic7XHJcbiAgICAgICAgb3V0cHV0LmlubmVySFRNTCArPSBuZXdCbG9ja1M7XHJcbiAgICAgICAgYmxvY2tMaXN0LnBvcCgpO1xyXG4gICAgICAgIGJsb2NrTGlzdC5wdXNoKHsgYmxvY2s6IG5ld0Jsb2NrUywgc3RhdGU6IFwiZmFsbGluZ1wiLCBzaXplOiBcIlNcIiB9KTtcclxuICAgICAgICBjdXJyZW50Q29sdW1uID0gMTtcclxuICAgICAgICBjdXJyZW50Um93ID0gMTY7XHJcblxyXG4gICAgfVxyXG4gICAgZWxzZSBpZiAoc2l6ZSA9PSAyKSB7XHJcbiAgICAgICAgbGV0IG5ld0Jsb2NrTSA9ICc8cmVjdCBpZD1cInRlc3RCbG9ja1wiIHg9XCIwXCIgeT1cIjBcIiB3aWR0aD1cIjEwMFwiIGhlaWdodD1cIjUwXCIgc3Ryb2tlPVwiYmxhY2tcIiBmaWxsPVwibm9uZVwiLz4nO1xyXG4gICAgICAgIG91dHB1dC5pbm5lckhUTUwgKz0gbmV3QmxvY2tNO1xyXG4gICAgICAgIGJsb2NrTGlzdC5wb3AoKTtcclxuICAgICAgICBibG9ja0xpc3QucHVzaCh7IGJsb2NrOiBuZXdCbG9ja00sIHN0YXRlOiBcImZhbGxpbmdcIiwgc2l6ZTogXCJNXCIgfSk7XHJcbiAgICAgICAgY3VycmVudENvbHVtbiA9IDE7XHJcbiAgICAgICAgY3VycmVudFJvdyA9IDE2O1xyXG4gICAgICAgIHJvdGF0ZWQgPSAxO1xyXG4gICAgfVxyXG4gICAgZWxzZSBpZiAoc2l6ZSA9PSAzKSB7XHJcbiAgICAgICAgbGV0IG5ld0Jsb2NrTCA9ICc8cmVjdCBpZD1cInRlc3RCbG9ja1wiIHg9XCIwXCIgeT1cIjBcIiB3aWR0aD1cIjEwMFwiIGhlaWdodD1cIjEwMFwiIHN0cm9rZT1cImJsYWNrXCIgZmlsbD1cIm5vbmVcIi8+JztcclxuICAgICAgICBvdXRwdXQuaW5uZXJIVE1MICs9IG5ld0Jsb2NrTDtcclxuICAgICAgICBibG9ja0xpc3QucG9wKCk7XHJcbiAgICAgICAgYmxvY2tMaXN0LnB1c2goeyBibG9jazogbmV3QmxvY2tMLCBzdGF0ZTogXCJmYWxsaW5nXCIsIHNpemU6IFwiTFwiIH0pO1xyXG4gICAgICAgIGN1cnJlbnRDb2x1bW4gPSAxO1xyXG4gICAgICAgIGN1cnJlbnRSb3cgPSAxNjtcclxuICAgIH1cclxufVxyXG4vLy9UZXN0IHN0dWZmIGRvZXNuJ3QgYWN0dWFsbHkgd29ya1xyXG5mdW5jdGlvbiB0ZXN0KGV2ZW50KXtcclxuICAgIGxldCBvdXRwdXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI291dHB1dFwiKTtcclxuICAgIGlmKGV2ZW50ID09IDQ5KXtcclxuICAgICAgICBibG9ja0xpc3QucG9wKCk7XHJcbiAgICAgICAgYmxvY2tMaXN0LnB1c2goeyBibG9jazogJzxyZWN0IGlkPVwidGVzdEJsb2NrU3RvcFwiIHg9XCIwXCIgeT1cIjBcIiB3aWR0aD1cIjUwXCIgaGVpZ2h0PVwiNTBcIiBzdHJva2U9XCJibGFja1wiIGZpbGw9XCJub25lXCIvPicsIHN0YXRlOiBcImZhbGxpbmdcIiwgc2l6ZTogXCJTXCIgfSk7XHJcbiAgICAgICAgb3V0cHV0LmlubmVySFRNTCA9IHN2Z1N0cmluZyArICc8cmVjdCBpZD1cInRlc3RCbG9ja1N0b3BcIiB4PVwiMTAwXCIgeT1cIjUwMFwiIHdpZHRoPVwiMTAwXCIgaGVpZ2h0PVwiMTAwXCIgc3Ryb2tlPVwiYmxhY2tcIiBmaWxsPVwibm9uZVwiLz4nICtcclxuICAgICAgICAgICAgICAgICAgJzxyZWN0IGlkPVwidGVzdEJsb2NrU3RvcFwiIHg9XCIxMDBcIiB5PVwiNjAwXCIgd2lkdGg9XCIxMDBcIiBoZWlnaHQ9XCIxMDBcIiBzdHJva2U9XCJibGFja1wiIGZpbGw9XCJub25lXCIvPicgK1xyXG4gICAgICAgICAgICAgICAgICAnPHJlY3QgaWQ9XCJ0ZXN0QmxvY2tTdG9wXCIgeD1cIjEwMFwiIHk9XCI3MDBcIiB3aWR0aD1cIjEwMFwiIGhlaWdodD1cIjEwMFwiIHN0cm9rZT1cImJsYWNrXCIgZmlsbD1cIm5vbmVcIi8+JyArXHJcbiAgICAgICAgICAgICAgICAgIGJsb2NrTGlzdFswXS5ibG9jaztcclxuICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2cob3V0cHV0KTtcclxuICAgIH1cclxuICAgIGVsc2UgaWYoZXZlbnQgPT0gNTApe1xyXG4gICAgICAgIGJsb2NrTGlzdC5wb3AoKTtcclxuICAgICAgICBibG9ja0xpc3QucHVzaCh7IGJsb2NrOiAnPHJlY3QgaWQ9XCJ0ZXN0QmxvY2tTdG9wXCIgeD1cIjBcIiB5PVwiMFwiIHdpZHRoPVwiNTBcIiBoZWlnaHQ9XCIxMDBcIiBzdHJva2U9XCJibGFja1wiIGZpbGw9XCJub25lXCIvPicsIHN0YXRlOiBcImZhbGxpbmdcIiwgc2l6ZTogXCJNXCIgfSk7XHJcbiAgICAgICAgb3V0cHV0LmlubmVySFRNTCA9IHN2Z1N0cmluZyArICc8cmVjdCBpZD1cInRlc3RCbG9ja1N0b3BcIiB4PVwiMTAwXCIgeT1cIjUwMFwiIHdpZHRoPVwiMTAwXCIgaGVpZ2h0PVwiMTAwXCIgc3Ryb2tlPVwiYmxhY2tcIiBmaWxsPVwibm9uZVwiLz4nICtcclxuICAgICAgICAnPHJlY3QgaWQ9XCJ0ZXN0QmxvY2tTdG9wXCIgeD1cIjEwMFwiIHk9XCI2MDBcIiB3aWR0aD1cIjEwMFwiIGhlaWdodD1cIjEwMFwiIHN0cm9rZT1cImJsYWNrXCIgZmlsbD1cIm5vbmVcIi8+JyArXHJcbiAgICAgICAgJzxyZWN0IGlkPVwidGVzdEJsb2NrU3RvcFwiIHg9XCIxMDBcIiB5PVwiNzAwXCIgd2lkdGg9XCIxMDBcIiBoZWlnaHQ9XCIxMDBcIiBzdHJva2U9XCJibGFja1wiIGZpbGw9XCJub25lXCIvPicgK1xyXG4gICAgICAgIGJsb2NrTGlzdFswXS5ibG9jaztcclxuICAgICAgICBjb25zb2xlLmxvZyhvdXRwdXQpO1xyXG5cclxuICAgIH1cclxuICAgIGVsc2UgaWYoZXZlbnQgPT0gNTEpe1xyXG4gICAgICAgIGJsb2NrTGlzdC5wb3AoKTtcclxuICAgICAgICBibG9ja0xpc3QucHVzaCh7IGJsb2NrOiAnPHJlY3QgaWQ9XCJ0ZXN0QmxvY2tTdG9wXCIgeD1cIjBcIiB5PVwiMFwiIHdpZHRoPVwiMTAwXCIgaGVpZ2h0PVwiMTAwXCIgc3Ryb2tlPVwiYmxhY2tcIiBmaWxsPVwibm9uZVwiLz4nLCBzdGF0ZTogXCJmYWxsaW5nXCIsIHNpemU6IFwiTFwiIH0pO1xyXG4gICAgICAgIG91dHB1dC5pbm5lckhUTUwgPSBzdmdTdHJpbmcgKyAnPHJlY3QgaWQ9XCJ0ZXN0QmxvY2tTdG9wXCIgeD1cIjEwMFwiIHk9XCI1MDBcIiB3aWR0aD1cIjEwMFwiIGhlaWdodD1cIjEwMFwiIHN0cm9rZT1cImJsYWNrXCIgZmlsbD1cIm5vbmVcIi8+JyArXHJcbiAgICAgICAgJzxyZWN0IGlkPVwidGVzdEJsb2NrU3RvcFwiIHg9XCIxMDBcIiB5PVwiNjAwXCIgd2lkdGg9XCIxMDBcIiBoZWlnaHQ9XCIxMDBcIiBzdHJva2U9XCJibGFja1wiIGZpbGw9XCJub25lXCIvPicgK1xyXG4gICAgICAgICc8cmVjdCBpZD1cInRlc3RCbG9ja1N0b3BcIiB4PVwiMTAwXCIgeT1cIjcwMFwiIHdpZHRoPVwiMTAwXCIgaGVpZ2h0PVwiMTAwXCIgc3Ryb2tlPVwiYmxhY2tcIiBmaWxsPVwibm9uZVwiLz4nICtcclxuICAgICAgICAgYmxvY2tMaXN0WzBdLmJsb2NrO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKG91dHB1dCk7XHJcbiAgICB9XHJcbiAgIFxyXG59XHJcblxyXG53aW5kb3cua2V5Q2hlY2sgPSBrZXlDaGVjazsiXSwic291cmNlUm9vdCI6IiJ9