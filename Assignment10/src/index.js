let blockList = [{ block: '<rect id="testBlock" x="0" y="0" width="50" height="50" stroke="black" fill="none"/>', state: "falling", size: "S" }
];
let svgString = '<rect width="400" height="800" stroke="black" fill="none"/>';
let currentColumn = 1;
let currentRow = 16;
let columns = [];
let rows = [];
let rotated = 1;
let score = 0;
function createArray() {
    for (l = 0; l < 10; l++) {
        columns.push({ Type: "", State: "", Status: "", Space: "E" })
    }
    columns[0].Space = "Wall";
    columns[9].Space = "Wall";
    for (m = 0; m < 17; m++) {
        rows.push(columns.slice(0));
    }
    rows[0].Space = "floor";
    console.log(rows, "new");

}
createArray();

let paused = false;
let gameOver = false;
//Checks what key is pressed and calls a function accordingly
function keyCheck(event) {
    let keyPressed = event.keyCode;
    console.log(keyPressed);
    if (keyPressed == 37) {
        moveHorizontal(37);
    }
    else if (keyPressed == 39) {
        moveHorizontal(39);
    }
    else if (keyPressed == 40) {
        moveDown();
    }
    else if (keyPressed == 32) {
        pauseGame();
    }
    else if (keyPressed == 65) {
        rotateBlock(65);
    }
    else if (keyPressed == 49) {
        test(49);
    }
    else if (keyPressed == 50) {
        test(50);
    }
    else if (keyPressed == 51) {
        test(51);
    }
}
//Pauses Game if space is pressed
function pauseGame(event) {
    if (paused == false) {
        paused = true;
    }
    else if (paused == true && gameOver == false) {
        paused = false;
    }
}

//Rotates block if possible
function rotateBlock(event) {
    let getWidth = document.querySelector("rect:last-of-type").getAttribute("width");
    let getHeight = document.querySelector("rect:last-of-type").getAttribute("height");
    let getBlock = document.querySelector("rect:last-of-type");
    let getY = document.querySelector("rect:last-of-type").getAttribute("y");
    let Y = Number(getY);
    let width = Number(getWidth);
    let height = Number(getHeight);
    let sub;
    if (event == 65) {
        if (rotated == 1 && rows[currentRow - 1][currentColumn].Space == "E") {
            sub = width;
            width = height;
            height = sub;
            getBlock.setAttribute("width", width);
            getBlock.setAttribute("height", height);
            rotated += 1;

        }
        else if (rotated == 2 && rows[currentRow][currentColumn + 1].Space == "E") {
            sub = width;
            width = height;
            height = sub;
            getBlock.setAttribute("width", width);
            getBlock.setAttribute("height", height);
            rotated += 1;
        }
        if (rotated == 3) {
            rotated = 1;
        }
    }
}
//Moves the block left or right depending on which key was pressed
function moveHorizontal(event) {
    let getX = document.querySelector("rect:last-of-type").getAttribute("x");
    let X = Number(getX);
    if (paused == false) {
        if (blockList[0].size == "S") {
            if (event == 37 && X > 0 && rows[currentRow][currentColumn - 1].Space == "E") {
                X -= 50;
                getX = document.querySelector("rect:last-of-type")
                getX.setAttribute("x", X);
                currentColumn -= 1;
            }
            else if (event == 39 && X < 350 && rows[currentRow][currentColumn + 1].Space == "E") {
                X += 50;
                getX = document.querySelector("rect:last-of-type")
                getX.setAttribute("x", X);
                currentColumn += 1;
            }
        }
        else if (blockList[0].size == "M") {
            if (rotated == 1) {
                if (event == 37 && X > 0 && rows[currentRow][currentColumn - 1].Space == "E") {
                    X -= 50;
                    getX = document.querySelector("rect:last-of-type")
                    getX.setAttribute("x", X);
                    currentColumn -= 1;
                }
                else if (event == 39 && X < 300 && rows[currentRow][currentColumn + 2].Space == "E") {
                    X += 50;
                    getX = document.querySelector("rect:last-of-type")
                    getX.setAttribute("x", X);
                    currentColumn += 1;

                }
            }
            if (rotated == 2) {
                if (event == 37 && X > 0 && rows[currentRow][currentColumn - 1].Space == "E" && rows[currentRow - 1][currentColumn - 1].Space == "E") {
                    X -= 50;
                    getX = document.querySelector("rect:last-of-type")
                    getX.setAttribute("x", X);
                    currentColumn -= 1;
                }
                else if (event == 39 && X < 350 && rows[currentRow][currentColumn + 1].Space == "E" && rows[currentRow - 1][currentColumn + 1].Space == "E") {
                    X += 50;
                    getX = document.querySelector("rect:last-of-type")
                    getX.setAttribute("x", X);
                    currentColumn += 1;
                }
            }
        }
        else if (blockList[0].size == "L") {
            if (event == 37 && X > 0 && rows[currentRow][currentColumn - 1].Space == "E" && rows[currentRow - 1][currentColumn - 1].Space == "E") {
                X -= 50;
                getX = document.querySelector("rect:last-of-type")
                getX.setAttribute("x", X);
                currentColumn -= 1;
            }
            else if (event == 39 && X < 300 && rows[currentRow][currentColumn + 2].Space == "E" && rows[currentRow - 1][currentColumn + 1].Space == "E") {
                X += 50;
                getX = document.querySelector("rect:last-of-type")
                getX.setAttribute("x", X);
                currentColumn += 1;
            }
        }
    }
}
//Moves the block down if down is pressed
function moveDown(event) {
    let getY = document.querySelector("rect:last-of-type").getAttribute("y");
    let Y = Number(getY);
    if (paused == false) {
        if (blockList[0].size == "S" && Y < 750 && rows[currentRow - 1][currentColumn].Space == "E") {
            Y += 50;
            getY = document.querySelector("rect:last-of-type");
            getY.setAttribute("y", Y);
            currentRow -= 1;
        }
        else if (blockList[0].size == "M" && rotated == 1 && Y < 750 && blockList[0].state == "falling" && rows[currentRow - 1][currentColumn].Space == "E" && rows[currentRow - 1][currentColumn + 1].Space == "E") {
            Y += 50;
            getY = document.querySelector("rect:last-of-type");
            getY.setAttribute("y", Y);
            currentRow -= 1;
        }
        else if (blockList[0].size == "M" && rotated == 2 && Y < 700 && blockList[0].state == "falling" && rows[currentRow - 2][currentColumn].Space == "E") {
            Y += 50;
            getY = document.querySelector("rect:last-of-type");
            getY.setAttribute("y", Y);
            currentRow -= 1;
            console.log("stop");
        }
        else if (blockList[0].size == "L" && blockList[0].state == "falling" && Y < 700 && rows[currentRow - 2][currentColumn].Space == "E" && rows[currentRow - 2][currentColumn + 1].Space == "E") {
            Y += 50;
            getY = document.querySelector("rect:last-of-type");
            getY.setAttribute("y", Y);
            currentRow -= 1;
        }

    }
}

//Automatically moves block down and calls the block creation vfunction iff the space below is occupied
setInterval(function autoMoveDown() {
    if (paused == false) {
        let getY = document.querySelector("rect:last-of-type").getAttribute("y");
        let getHeight = document.querySelector("rect:last-of-type").getAttribute("height");
        let Y = Number(getY);

        if (blockList[0].size == "S" && Y < 750 && blockList[0].state == "falling" && rows[currentRow - 1][currentColumn].Space == "E") {
            Y += 50;
            getY = document.querySelector("rect:last-of-type");
            getY.setAttribute("y", Y);
            currentRow -= 1;
        }
        else if (blockList[0].size == "M" && rotated == 1 && Y < 750 && blockList[0].state == "falling" && rows[currentRow - 1][currentColumn].Space == "E" && rows[currentRow - 1][currentColumn + 1].Space == "E") {
            Y += 50;
            getY = document.querySelector("rect:last-of-type");
            getY.setAttribute("y", Y);
            currentRow -= 1;
        }
        else if (blockList[0].size == "M" && rotated == 2 && Y < 700 && blockList[0].state == "falling" && rows[currentRow - 2][currentColumn].Space == "E") {
            Y += 50;
            getY = document.querySelector("rect:last-of-type");
            getY.setAttribute("y", Y);
            currentRow -= 1;
            console.log("stop");
        }

        else if (blockList[0].size == "L" && blockList[0].state == "falling" && Y < 700 && rows[currentRow - 2][currentColumn].Space == "E" && rows[currentRow - 2][currentColumn + 1].Space == "E") {
            Y += 50;
            getY = document.querySelector("rect:last-of-type");
            getY.setAttribute("y", Y);
            currentRow -= 1;
        }
        else {
            
            updateArray();
            drawNewBlock();
        }
    }
}, 1000)

//Updates the 2d array
function updateArray() {
    let last = document.querySelector("rect:last-of-type");

    if (blockList[0].size == "S") {
        rows[currentRow][currentColumn] = { block: last, state: "stopped", size: "S", Space: "O" };
    }
    else if (blockList[0].size == "M") {
        if (rotated == 1) {
            rows[currentRow][currentColumn] = { block: last, state: "stopped", size: "M", Space: "O" };
            rows[currentRow][currentColumn + 1] = { block: last, state: "stopped", size: "M", Space: "O" };
        }
        else if (rotated == 2) {
            rows[currentRow][currentColumn] = { block: last, state: "stopped", size: "M", Space: "O" };
            rows[currentRow - 1][currentColumn] = { block: last, state: "stopped", size: "M", Space: "O" };
        }
        else if (rotated == 3) {
            rows[currentRow][currentColumn] = { block: last, state: "stopped", size: "M", Space: "O" };
            rows[currentRow][currentColumn - 1] = { block: last, state: "stopped", size: "M", Space: "O" };
        }
        else if (rotated == 4) {
            rows[currentRow][currentColumn] = { block: last, state: "stopped", size: "M", Space: "O" };
            rows[currentRow + 1][currentColumn] = { block: last, state: "stopped", size: "M", Space: "O" };
        }
    }
    else if (blockList[0].size == "L") {
        rows[currentRow][currentColumn] = { block: last, state: "stopped", size: "L", Space: "O" };
        rows[currentRow][currentColumn + 1] = { block: last, state: "stopped", size: "L", Space: "O" };
        rows[currentRow - 1][currentColumn] = { block: last, state: "stopped", size: "L", Space: "O" };
        rows[currentRow - 1][currentColumn + 1] = { block: last, state: "stopped", size: "L", Space: "O" };
    }
    for (let r = 0; r < 2; r++) {
        if (rows[r].Space == "O") {
            rows[r].Space == "E";
            rows[r].Type == "";
            rows.State = "falling";
        }
    }
    removeLines();
    endGame();
}

function removeLines() {
    let filledSpots = 0;
    let blockString = ''
    for (let r = 15; r > 0; r--) {
        for (let c = 0; c < 10; c++) {
            if (rows[r][c].Space == "O") {
                filledSpots += 1;
            
            if (filledSpots == 8) {
                rows.splice(r, 1);
                score += 1;
                console.log(rows);
                for (let r = 15; r > 0; r--) {
                    for (let b = 0; b < 10; c++) {
                        blockString += rows[r][b].block;
                    }
                }
                        console.log(blockString);
                        let output = document.querySelector("#output");
                        output.innerHTML = svgString + blockString;
                        console.log(output);
                    
                
            }
            else{
                console.log(filledSpots);
                filledSpots = 0;
            }
        }
            
        }

    }
}

function endGame() {
    for (o = 1; o < columns.length; o++) {
        if (rows[15][o].Space == "O") {
            gameOver = true;
            paused = true;
            break
        }
    }
}
//Draws new blocks
function drawNewBlock() {
    let output = document.querySelector("#output");
    let size = Math.floor((Math.random() * 3) + 1);
    if (size == 1) {
        let newBlockS = '<rect id="testBlock" x="0" y="0" width="50" height="50" stroke="black" fill="none"/>';
        output.innerHTML += newBlockS;
        blockList.pop();
        blockList.push({ block: newBlockS, state: "falling", size: "S" });
        currentColumn = 1;
        currentRow = 16;

    }
    else if (size == 2) {
        let newBlockM = '<rect id="testBlock" x="0" y="0" width="100" height="50" stroke="black" fill="none"/>';
        output.innerHTML += newBlockM;
        blockList.pop();
        blockList.push({ block: newBlockM, state: "falling", size: "M" });
        currentColumn = 1;
        currentRow = 16;
        rotated = 1;
    }
    else if (size == 3) {
        let newBlockL = '<rect id="testBlock" x="0" y="0" width="100" height="100" stroke="black" fill="none"/>';
        output.innerHTML += newBlockL;
        blockList.pop();
        blockList.push({ block: newBlockL, state: "falling", size: "L" });
        currentColumn = 1;
        currentRow = 16;
    }
}
///Test stuff doesn't actually work
function test(event){
    let output = document.querySelector("#output");
    if(event == 49){
        blockList.pop();
        blockList.push({ block: '<rect id="testBlockStop" x="0" y="0" width="50" height="50" stroke="black" fill="none"/>', state: "falling", size: "S" });
        output.innerHTML = svgString + '<rect id="testBlockStop" x="100" y="500" width="100" height="100" stroke="black" fill="none"/>' +
                  '<rect id="testBlockStop" x="100" y="600" width="100" height="100" stroke="black" fill="none"/>' +
                  '<rect id="testBlockStop" x="100" y="700" width="100" height="100" stroke="black" fill="none"/>' +
                  blockList[0].block;
                  console.log(output);
    }
    else if(event == 50){
        blockList.pop();
        blockList.push({ block: '<rect id="testBlockStop" x="0" y="0" width="50" height="100" stroke="black" fill="none"/>', state: "falling", size: "M" });
        output.innerHTML = svgString + '<rect id="testBlockStop" x="100" y="500" width="100" height="100" stroke="black" fill="none"/>' +
        '<rect id="testBlockStop" x="100" y="600" width="100" height="100" stroke="black" fill="none"/>' +
        '<rect id="testBlockStop" x="100" y="700" width="100" height="100" stroke="black" fill="none"/>' +
        blockList[0].block;
        console.log(output);

    }
    else if(event == 51){
        blockList.pop();
        blockList.push({ block: '<rect id="testBlockStop" x="0" y="0" width="100" height="100" stroke="black" fill="none"/>', state: "falling", size: "L" });
        output.innerHTML = svgString + '<rect id="testBlockStop" x="100" y="500" width="100" height="100" stroke="black" fill="none"/>' +
        '<rect id="testBlockStop" x="100" y="600" width="100" height="100" stroke="black" fill="none"/>' +
        '<rect id="testBlockStop" x="100" y="700" width="100" height="100" stroke="black" fill="none"/>' +
         blockList[0].block;
        console.log(output);
    }
   
}

window.keyCheck = keyCheck;