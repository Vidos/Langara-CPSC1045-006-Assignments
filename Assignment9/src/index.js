let dictionary = [{Word: "Dog", Definition: "A person regarded as unplesant, contemptible, or wicked."}, 
{Word: "Cat", Definition: "Ninjas in fut suits with knives hiddden in the paws."}, 
{Word: "Door", Definition: "A device to walk through walls."}, 
{Word: "Windows", Definition: "A PC based operating system by Microsoft."}, 
{Word: "Car", Definition: "2-ton steel carriages powered by explosions, made out of dinosaurs which are used for transportation."}];


function searchDictionary(){
    let search = document.querySelector("#term").value;
    let word = document.querySelector("#word");
    search = search.charAt(0).toUpperCase() + search.slice(1);
    for(let element of dictionary){
        if(search == element.Word){
            let wordDefinition = document.querySelector("#wordDefinition");
            word.innerHTML = element.Word;
            wordDefinition.innerHTML = element.Definition;
            break;
        }
        else{
            word.innerHTML = "That word is not in the dictionary";
            wordDefinition.innerHTML = "";
        }
    }
}
window.searchDictionary = searchDictionary;