
let baseString = "TestPage"
function populatePage() {
    let containers = document.querySelectorAll(".container"); //get all the containers

    //Part 1: Rewrite the following code using a for/of loop. 
    //Trivial example: This code capitalizes what is in the HTML file
    //and counts the number of charcters
    //reference for/of loops: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...of
    //reference for loops: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for
    let count = 0;
    let i = 0;

    for(value of containers){
        change();
    }

    function change(){
        
        count = count + containers[i].innerHTML.length;
        let par = String(containers[i].innerHTML);
        par = par.toUpperCase();
        containers[i].innerHTML = par;
        i += 1;
    }

    let output = document.querySelector("#output");
    output.innerHTML = "Number of characters:" + count;

    let heights = [4, 5, 3, 7, 6, 10,-2];
    let pointString = "";
    let x = 0;
    let i2 = 0;
    for(value of heights){
        pts();
    }

    function pts(){
        x = x + 10;
        y = 200 - heights[i2] * 20;
        pointString = pointString + x + "," + y + " ";
        i2 += 1;
    }
   
    let output2 = document.querySelector("#output2");
    output2.setAttribute("points", pointString);


    let newThings = []
    let data = [30, 70, 110, 150];
    let i3 = 0;
    let svgString = '<svg width="500" height="500">';
    x= 0;

    for(value of data){
        makeCircle();
    }

    function makeCircle(){
        x = data[i3];
        circleString = '<circle cx="'+x+'"'+' cy="'+250+'" r="'+20+'" fill="black" />';
        svgString += circleString;
        i3 += 1;
    }

    svgString += "</svg>"
    let output3 = document.querySelector("#output3")
    output3.innerHTML += svgString;

}





