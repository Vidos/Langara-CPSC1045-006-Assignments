function startChange(){
let sizeB = document.querySelector("#initialSize");
let growthB = document.querySelector("#growth");
let BoxesB = document.querySelector("#boxNum");
let size = Math.abs(Number(sizeB.value));
let Boxes = Math.abs(Number(BoxesB.value)); 
let growth = Math.abs(Number(growthB.value));
let x = 0;
let svgString = '<svg width="1000" height="300">';

function makeBoxes(){
boxString = '<rect x="' + x + '"' + ' y="' + 0 + '"' + ' width="' + size + '"' + ' height="' + size + '"' + ' fill="black"/>';
svgString += boxString;
}

for(let i = 0; i < Boxes; i++){
    makeBoxes();
    size = size + growth;
    x = x + size + 10;
}

svgString += "</svg>";
let output = document.querySelector("#output");
output.innerHTML = svgString;
}
window.startChange = startChange;